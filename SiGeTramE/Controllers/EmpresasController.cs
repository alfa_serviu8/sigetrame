﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Entidades;
using SiGeTramDB.Dao;
using SiGeTramDB.Bo;

using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;

namespace SiGeTramE.Controllers
{
    public class EmpresasController : ApiController
    {
         EmpresaBo eBo= new EmpresaBoImp(new EmpresaDaoImp());

        [HttpGet]
        [Route("api/empresas")]
        public IHttpActionResult Listado()
        {
            

            return Ok(new { pagina = "" });

        }

        [HttpPost]
        [Route("api/empresas/listado")]
        public IHttpActionResult Listado(EmpresaBuscada eb)
        {
            int i = 0;
          
            return Ok(new { pagina = eBo.Listado(eb) });

        }



        [HttpPost]
        [Route("api/empresas")]
        public IHttpActionResult Agregar(Empresa e)
        {
            eBo.Agregar(e);

            return Ok();

        }


        [HttpPut]
        [Route("api/empresas")]
        public IHttpActionResult Actualizar(Empresa e)
        {
            eBo.Actualizar(e);

            return Ok() ;

        }

    }
}
