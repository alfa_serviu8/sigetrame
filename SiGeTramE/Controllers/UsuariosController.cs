﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramE.Models.Entidades;
using SiGeTramE.Models.Dao;
using SiGeTramE.Models.Bo;


namespace SiGeTramE.Controllers
{
    public class UsuariosController : ApiController
    {
        private UsuarioDao usuarioDao = new UsuarioDaoImp();
        private UsuarioBo usuarioBo = new UsuarioBoImp();

        [HttpGet]
        [Route("api/usuarios/{rut}")]
        public List<Usuario> PorRut(int rut)
        {
            return usuarioDao.PorRut(rut);
        }
               

        [HttpGet]
        [Route("api/usuarios/login")]
        public Usuario PorLogin()
        {
           string login = User.Identity.Name;

            login = login.Substring(9);

            return usuarioBo.PorLogin(login);

            
        }
         
        
      


    }
}
