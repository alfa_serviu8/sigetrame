﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTramE.Controllers
{
    public class ExpedientesController : ApiController
    {
        ExpedienteBo eBo = new ExpedienteBoImp();

        [HttpPost]
        [Route("api/expedientes/listado")]
        public IHttpActionResult Listado(ExpedienteBuscado eb)
        {
           return Ok(new { pagina = eBo.Listado(eb) });
        }

        [HttpPost]
        [Route("api/expedientes/")]
        public IHttpActionResult Post(Expediente e)
        {
           int id_Expediente = eBo.Agregar(e);

            return Ok(new { IdExpediente = id_Expediente });
        }

        [HttpPut]
        [Route("api/expedientes/")]
        public void Put(Expediente e)
        {
            eBo.Actualizar(e);
        }

        [HttpPut]
        [Route("api/expedientes/enviar")]
        public void Enviar(Expediente e)
        {
            eBo.Enviar(e);
        }

        //[HttpPut]
        //[Route("api/expedientes/eliminar")]
        //public IHttpActionResult Eliminar(Expediente expediente)
        //{
        //    bool expedienteEliminado;
        //    string mensajeResultado;
        //    //expediente.Eliminar(out expedienteEliminado, out mensajeResultado);

        //    //return Ok(new { expedienteEliminado= expedienteEliminado, mensajeResultado = mensajeResultado });
        //    return Ok();
        //}

        [HttpDelete]
        [Route("api/expedientes/{id}")]
        public IHttpActionResult Eliminar(int id)
        {
            eBo.Eliminar(id);

            return Ok();
        }


    }
}
