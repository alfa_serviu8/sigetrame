﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTramE.Controllers
{
    public class TipoExpedientesController : ApiController
    {
        TipoExpedienteBo teBo = new TipoExpedienteBoImp();

        [HttpPost]
        [Route("api/tipoExpedientes/listado")]
        public IHttpActionResult Listado(TipoExpedienteBuscado teb)
        {
            return Ok(new { pagina= teBo.Listado(teb) });
        }

        [HttpPost]
        [Route("api/tipoExpedientes/")]
        public void Post(TipoExpediente te)
        {
            teBo.Agregar(te);
        }

        [HttpPut]
        [Route("api/tipoExpedientes/")]
        public void Put(TipoExpediente te)
        {
            teBo.Actualizar(te);
        }
    }
}