﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTramE.Controllers
{
    public class TipologiaController : ApiController
    {
        TipologiaBo tBo = new TipologiaBoImp();

        [HttpPost]
        [Route("api/tipologias/listado")]
        public IHttpActionResult Listado(TipologiaBuscada tb)
        {
            

            return Ok(new { pagina = tBo.Listado(tb)});
        }

        [HttpPost]
        [Route("api/tipologias/")]
        public void Post(Tipologia t)
        {
            tBo.Agregar(t);
        }
      
        [HttpPut]
        [Route("api/tipologias/")]
        public void Put(Tipologia t)
        {
            tBo.Actualizar(t);
        }
    }
}
