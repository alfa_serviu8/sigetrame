﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Entidades;
using SiGeTramDB.Bo;

namespace SiGeTramE.Controllers
{
    public class TipoTramiteController : ApiController
    {
        TipoTramiteBo ttBo = new TipoTramiteBoImp();

        [HttpPost]
        [Route("api/tipoTramite/listado")]
        public IHttpActionResult Listado(TipoTramiteBuscado ttb)
        {

            return Ok(new { pagina = ttBo.Listado(ttb) });
        }


    }
}
