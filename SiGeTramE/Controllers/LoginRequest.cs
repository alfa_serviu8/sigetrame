﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SiGeTramE.Models.Dao;
using SiGeTramE.Models.Entidades;
namespace SiGeTramE.Controllers
{
    public class LoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public bool validaClave()
        {
            bool claveOk = false;

       
            UsuarioDao ud = new UsuarioDaoImp();

            // esta debiera ser la empresa //

            int rutEntero = Convert.ToInt32(Username.Substring(0, Username.Length - 2));


            List<Usuario> us = ud.PorRut(rutEntero);

            if (us.Count > 0)
            {
                if (us[0].Clave == Password)
                {
                    claveOk = true;

                }
            }

            return claveOk;
        }

        public bool userNameRegistrado()
        {
            bool userNameOk = false;


            UsuarioDao ud = new UsuarioDaoImp();

            // esta debiera ser la empresa //

            int rutEntero = Convert.ToInt32(Username.Substring(0, Username.Length - 2));


            List<Usuario> us = ud.PorRut(rutEntero);

            if (us.Count > 0)
            {

                userNameOk = true;

                
            }

            return userNameOk;
        }

        public void actualizarClave()
        {
           

            UsuarioDao ud = new UsuarioDaoImp();

            // esta debiera ser la empresa //

            int rutEntero = Convert.ToInt32(Username.Substring(0, Username.Length - 2));

                       
            List<Usuario> us = ud.PorRut(rutEntero);

            

            if (us.Count > 0)
            {

                us[0].Clave = Password;

                ud.Actualizar(us[0]);


            }

        }

    }

}