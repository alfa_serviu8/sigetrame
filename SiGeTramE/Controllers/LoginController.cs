﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace SiGeTramE.Controllers
{
        [AllowAnonymous]
        [RoutePrefix("api/login")]
        public class LoginController : ApiController
        {
               

            [HttpGet]
            [Route("echoping")]
            public IHttpActionResult EchoPing()
            {
                return Ok(true);
            }

            [HttpGet]
            [Route("echouser")]
            public IHttpActionResult EchoUser()
            {
                var identity = Thread.CurrentPrincipal.Identity;
                return Ok($" IPrincipal-user: {identity.Name} - IsAuthenticated: {identity.IsAuthenticated}");
            }

            [HttpPost]
            [Route("authenticate")]
            public IHttpActionResult Authenticate(LoginRequest login)
            {
                if (login == null)
                    throw new HttpResponseException(HttpStatusCode.BadRequest);
          
            //TODO: Validate credentials Correctly, this code is only for demo !!

             
                                      

            if (login.validaClave())
                {
                    var token = TokenGenerator.GenerateTokenJwt(login.Username);
                    return Ok(token);
                }
                else
                {
                    return Ok();
                }
            }

        [HttpPut]
        public IHttpActionResult ActualizarClave(LoginRequest login)
        {
            if (login == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            //TODO: Validate credentials Correctly, this code is only for demo !!

            login.actualizarClave();

            return Ok( );
           
        }

        [HttpPost]
        [Route("userNameRegistrado")]
        public IHttpActionResult VerificarUsername(LoginRequest login)
        {
            if (login == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            //TODO: Validate credentials Correctly, this code is only for demo !!

            return Ok(new { usuarioRegistrado = login.userNameRegistrado() });

        }


    }
}
