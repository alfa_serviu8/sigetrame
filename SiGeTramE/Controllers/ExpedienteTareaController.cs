﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTramE.Controllers
{
    public class ExpedienteTareaController : ApiController
    {
        ExpedienteTareaBo etBo = new ExpedienteTareaBoImp();

        [HttpPost]
        [Route("api/expedienteTarea/listado")]
        public IHttpActionResult Listado(ExpedienteTareaBuscada etb)
        {
           return Ok(new { pagina = etBo.Listado(etb) });
        }

        [HttpPost]
        [Route("api/expedienteTarea/")]
        public IHttpActionResult Post(ExpedienteTarea et)
        {
            etBo.Agregar(et);

            return Ok();
        }

        //[HttpPut]
        //[Route("api/expedientes/")]
        //public void Put(ExpedienteTarea et)
        //{
        //    etBo.Actualizar(et);
        //}

       

        [HttpPut]
        [Route("api/expedienteTarea/recepcionar")]
        public void Recepcionar(ExpedienteTarea et)
        {
            etBo.Recepcionar(et);
        }

        [HttpPut]
        [Route("api/expedienteTarea/observar")]
        public void Observar(ExpedienteTarea et)
        {
            etBo.Observar(et);
        }

        [HttpPut]
        [Route("api/expedienteTarea/modificar")]
        public void Modificar(ExpedienteTarea et)
        {
            etBo.Modificar(et);
        }

        //[HttpPut]
        //[Route("api/expedientes/eliminar")]
        //public IHttpActionResult Eliminar(Expediente expediente)
        //{
        //    bool expedienteEliminado;
        //    string mensajeResultado;
        //    //expediente.Eliminar(out expedienteEliminado, out mensajeResultado);

        //    //return Ok(new { expedienteEliminado= expedienteEliminado, mensajeResultado = mensajeResultado });
        //    return Ok();
        //}



    }
}
