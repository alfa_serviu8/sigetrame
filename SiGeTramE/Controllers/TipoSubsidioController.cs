﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Bo;
using SiGeTramDB.Entidades;

namespace SiGeTramE.Controllers
{
    public class TipoSubsidioController : ApiController
    {
        TipoSubsidioBo tsBo = new TipoSubsidioBoImp();

        [HttpPost]
        [Route("api/tipoSubsidio/listado")]
        public IHttpActionResult Listado(TipoSubsidioBuscado tsb)
        {
        
            return Ok(new { pagina = tsBo.Listado(tsb) });
        }

        [HttpPost]
        [Route("api/tipoSubsidio/")]
        public void Post(TipoSubsidio ts)
        {
            tsBo.Agregar(ts);
        }
      
        [HttpPut]
        [Route("api/tipoSubsidio/")]
        public void Put(TipoSubsidio ts)
        {
            tsBo.Actualizar(ts);
        }
    }
}
