﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using SiGeTramDB.Entidades;
using SiGeTramDB.Dao;
using SiGeTramDB.Bo;

namespace SiGeTramE.Controllers
{
    public class DocumentosController : ApiController
    {
        DocumentoBo dBo = new DocumentoBoImp();

        [HttpGet]
        [Route("api/documentos/{idDocumento}")]
        public HttpResponseMessage Get(int idDocumento)
        {

            HttpResponseMessage Response = new HttpResponseMessage(HttpStatusCode.OK);

           

            Response.Content = new ByteArrayContent(dBo.Contenido(idDocumento));

            //Response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");

            Response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

            return Response;
        }



        [HttpPost()]
        public IHttpActionResult SubirDocumento()
        {
            int iUploadedCnt = 0;

            List<int> claves = new List<int>();

         
            int id_expediente = Convert.ToInt32(System.Web.HttpContext.Current.Request.Form["id_expediente"]);

            string respuesta = "";

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                   

                        byte[] imageBytes = new byte[hpf.InputStream.Length + 1];

                        hpf.InputStream.Read(imageBytes, 0, imageBytes.Length);

                    // guarda en la base de datos

                    var documento = new Documento(0,
                        hpf.FileName.ToString().Trim(),
                        hpf.ContentType.ToString(),
                        imageBytes,
                        imageBytes.Length,
                        id_expediente

                    );

                    dBo.Agregar(documento, out respuesta);


                    iUploadedCnt = iUploadedCnt + 1;
                    
                }
            }
             
            // guardo los 


            // RETURN A MESSAGE (OPTIONAL).
            if (iUploadedCnt > 0)
            {
                return Ok(new { mensaje = iUploadedCnt + " Archivos Subidos Exitosamente",   claves = claves  }); 
            }
            else
            {
                return Ok(new { mensaje = " La subida falló"});
            }
        }


        [HttpPost]
        [Route("api/documentos/listado")]
        public IHttpActionResult Listado(DocumentoBuscado d)
        {



            int i = 0;

            return Ok(new { pagina = dBo.Listado(d) });

        }



        [HttpDelete]
        [Route("api/documentos/{id}")]
        public IHttpActionResult Delete(int id)
        {
            dBo.Eliminar(id);

            return Ok();
        }




    }
}
