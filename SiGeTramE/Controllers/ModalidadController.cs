﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SiGeTramDB.Dao;
using SiGeTramDB.Entidades;

namespace SiGeTramE.Controllers
{
    public class ModalidadController : ApiController
    {
        [HttpPost]
        [Route("api/modalidades/listado")]
        public IHttpActionResult Listado(ModalidadDaoImp dao)
        {
            return Ok(new { total_registros = dao.TotalRegistros(), listado = dao.Listado() });
        }

        [HttpPost]
        [Route("api/modalidades/")]
        public void Post(Modalidad modalidad)
        {
            //modalidad.Agregar();
        }
      
        [HttpPut]
        [Route("api/modalidades/")]
        public void Put(Modalidad modalidad)
        {
            //modalidad.Actualizar();
        }
    }
}
