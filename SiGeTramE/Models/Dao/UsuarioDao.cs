﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramE.Models.Entidades;

namespace SiGeTramE.Models.Dao
{
    public interface UsuarioDao
    {
        List<Usuario> PorRut(int rut);
        List<Usuario> PorLogin(string login);
        void Actualizar(Usuario u);
            

    }
}
