﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTramE.Models.Entidades
{
    public class Usuario
    {
        public int Rut { set; get; }
        public string  Dv { set; get; }
        public string  Nombres { set; get; }
        public string  Paterno { set; get; }
        public string  Materno { set; get; }
        public string  NombreCompleto { set; get; }
        public int Id_Genero { set; get; }
        public string  Email_Particular { set; get; }
        public string Email { set; get; }
        public int    Id_Servicio { set; get; }
        public int Id_Region { set; get; }
        public int    Id_Dependencia { set; get; }
        public int    Id_Provincia { set; get; }
        public string Nombre_Servicio { set; get; }
        public string Nombre_Largo_Servicio { set; get; }
        public string Direccion_Servicio { set; get; }
        public string Telefono_Servicio { set; get; }
        public int    Id_Tipo_Excepcion { set; get; }
        public string Nombre_Tipo_Excepcion { set; get; }
        public string Logo_Servicio { set; get; }
        public string Mosca { set; get; }
        public string Clave { set; get; }
        public int Id_Calidad_Juridica { set; get; }
        SqlConnection con = new ConeccionComun().coneccion();

       

       


       
        public void Actualizar()
        {
            int respuesta = 0;
            string sActualizar = "Update dbo.Cliente " +
                "Set Pass_Sistemas =@Clave " +
                "Where Rut=@Rut";


            try
            {
                using (con)
                {
                    con.Open();

                    var query = new SqlCommand(sActualizar, con);



                    query.CommandType = CommandType.Text;

                    query.Parameters.Add(new SqlParameter("@Rut", this.Rut));
                    query.Parameters.Add(new SqlParameter("@Clave", this.Clave));
                   
                    respuesta = query.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }


    
}