﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SiGeTramE.Models.Entidades;
using SiGeTramE.Models.Dao;

namespace SiGeTramE.Models.Bo
{
    public class UsuarioBoImp:UsuarioBo
    {

        UsuarioDao usuario_DAO = new UsuarioDaoImp();
                      
        List<Usuario> listadoUsuario = new List<Usuario>();
                 

        public Usuario PorLogin(string login)
        {
            listadoUsuario = usuario_DAO.PorLogin(login);
                                   
           

            Usuario usuario  = new Usuario();

            if (listadoUsuario.Count > 0)
            {
                usuario = listadoUsuario[0];

               
               
            }

            return usuario;
           
        }

        public Usuario PorRut(int rut)
        {
            listadoUsuario = usuario_DAO.PorRut(rut);



            Usuario usuario = new Usuario();

            if (listadoUsuario.Count > 0)
            {
                usuario = listadoUsuario[0];

                
            }

            return usuario;

        }

        public void Actualizar(Usuario u)
        {

            usuario_DAO.Actualizar(u);

        }

    }
    }
