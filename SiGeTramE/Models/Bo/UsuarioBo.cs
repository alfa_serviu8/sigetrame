﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiGeTramE.Models.Entidades;


namespace SiGeTramE.Models.Bo
{
    public interface UsuarioBo
    {
        Usuario PorLogin(string login);

        Usuario PorRut(int rut);

        void Actualizar(Usuario u);




    }
}
