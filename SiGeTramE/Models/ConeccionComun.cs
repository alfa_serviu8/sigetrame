﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SiGeTramE.Models
{
    public class ConeccionComun
    {
        public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Comun_Sistemas"].ToString());
             

        public SqlConnection coneccion() 
        {
            return this.con;
            

        }

        
    }
}