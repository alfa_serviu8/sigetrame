﻿app.factory("tareasCamundaSrv", ["$http", "urlApi", TareasCamundaSrv]);

function TareasCamundaSrv($http, urlApi) {

    var ruta = urlApi + "tareasCamunda/";



    var completar = function (data) {

        return $http.post(ruta + "completar", data);
    };




    return {
        completar: completar

    };
};