﻿app.controller("tareasCtrl", ["$scope", "tareasSrv", "tareasProcesoSrv","usuariosSrv",  "$location", TareasCtrl]);

function TareasCtrl($scope, tareasSrv, tareasProcesoSrv, usuariosSrv,  $location) {
 
    $scope.tareas = [];

    
    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();

    };

    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.filtro = angular.copy(tareasSrv.filtroActual());

        $scope.filtro.tareaBuscada.Rut_Asignado = usuariosSrv.usuarioActual().Rut;

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;

    }); // fin usuario


    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    };
    
    $scope.listadoPaginado = function (filtroActual) {

        tareasSrv.listado(filtroActual).then(function (respuesta) {

            $scope.tareas = respuesta.data.listado;

                     
            $scope.bigTotalItems = respuesta.data.total_registros;
        });
    };


    $scope.editar_tarea = function (t) {

        tareasProcesoSrv.ruta_formulario(t.Id_Tarea).then(function (respuesta) {
           
            $location.path("tarea/" + t.Id_Tarea + "/" + respuesta.data.key);

        });

    };



}


app.controller("tareaRevisorCtrl", ["$scope", "expedientesSrv", "tareasProcesoSrv", "tareasSrv", "usuariosSrv", "$routeParams", "$location", "documentosSrv", TareaRevisorCtrl]);

function TareaRevisorCtrl($scope, expedientesSrv, tareasProcesoSrv, tareasSrv, usuariosSrv, $routeParams, $location, documentosSrv) {
          

    $scope.tarea = {};

    var operador = {};
    
    var expediente = {};





    $scope.filtro = {
        documento_buscado: {
            "Id_Expediente": 0

        },
        "numero_pagina": "1",
        "tamacno_pagina": "10"

    };


    $scope.listadoPaginado = function (filtro_actual) {

        var filtro = filtro_actual;

        documentosSrv.guardarFiltro(filtro);


        documentosSrv.listado(filtro).then(function (respuesta) {

            $scope.documentos = respuesta.data.listado;

            $scope.bigTotalItems = respuesta.data.total_registros;


        });

    };



          




    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        
    };

    $scope.esTab = function (tab) {

        return $scope.tab == tab;

    };

    $scope.estableceTab('Descripcion');




    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);
       

        tareasSrv.porId($routeParams.idTarea).then(function (respuesta) {

            $scope.tarea = respuesta.data.listado[0];

               

                expedientesSrv.porId($scope.tarea.Id_Expediente).then(function (respuesta) {

                    $scope.expediente = respuesta.data.listado[0];

                    expediente = new Expediente(respuesta.data.listado[0]);

                    console.log("El expediente es");

                    console.log($scope.expediente);

                    usuariosSrv.porRut($scope.expediente.Rut_Operador).then(function (respuesta) {

                        operador = respuesta.data;

                        console.log("Este es el operador");

                        console.log(operador);


                    });



                    // cargo el filtro


                    $scope.filtro.documento_buscado.Id_Expediente = $scope.expediente.Id_Expediente;

                    $scope.listadoPaginado($scope.filtro);
                   


                });
        });

       
        


    });

     
    $scope.recepcionar = function () {

        if ($scope.tarea.Observaciones === undefined) {

            $scope.tarea.Observaciones = "Sin Observaciones";

        }

        $scope.tarea.Id_Estado_Expediente = 4;

        $scope.expediente.Id_Estado_Expediente = 4;

        expedientesSrv.actualizar($scope.expediente).then(function (respuesta) {  
            console.log("esta es la respuesta al actualizar el expediente");
            console.log(respuesta);
            

            tareasSrv.agregar($scope.tarea).then(function (respuesta) {
                console.log("esta es la respuesta al agregar la tarea");
                console.log(respuesta);


                var correo = new CorreoRecepcionado(new CorreoSPPV(usuariosSrv.usuarioActual(), operador, expediente)).json();

                console.log("este es el correro");
                console.log(correo);
                console.log("este es el expediente como tabla");
                console.log(expediente.string());

                var data = {
                    "variables": {
                        "Id_Estado_Expediente": { "value": $scope.expediente.Id_Estado_Expediente.toString(), "type": "String" },
                        "correo_recepcionado": { "value": JSON.stringify(correo), "type": "String" }
                    }
                };

                tareasProcesoSrv.completar($scope.tarea.Id_Tarea, data).then(function (respuesta) {

                    $location.path("/tareas");

                });

            });// fin agregar tarea

        });// fin actualizar expediente

    };

    $scope.observar = function () {



        if ($scope.tarea.Observaciones === undefined) {

            $scope.tarea.Observaciones = "Sin Observaciones";

        }

        $scope.tarea.Id_Estado_Expediente = 3;

        $scope.expediente.Id_Estado_Expediente = 3;

        expedientesSrv.actualizar($scope.expediente).then(function (respuesta) {
            console.log("esta es la respuesta al actualizar el expediente");
            console.log(respuesta);


            tareasSrv.agregar($scope.tarea).then(function (respuesta) {
                console.log("esta es la respuesta al agregar la tarea");
                console.log(respuesta);


                var correo = new CorreoObservado(new CorreoSPPV(usuariosSrv.usuarioActual(), operador, expediente)).json();

                console.log("este es el correro");
                console.log(correo);
                console.log("este es el expediente como tabla");
                console.log(expediente.string());

                var data = {
                    "variables": {
                        "Id_Estado_Expediente": { "value": $scope.expediente.Id_Estado_Expediente.toString(), "type": "String" },
                        "correo_observado": { "value": JSON.stringify(correo), "type": "String" }
                    }
                };

                tareasProcesoSrv.completar($scope.tarea.Id_Tarea, data).then(function (respuesta) {

                    $location.path("/tareas");

                });

            });// fin agregar tarea

        });// fin actualizar expediente

      
                
    };


    // maejo de los documentos




    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.nmero_pagina = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };


    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();


    };


    //usuariosSrv.porLogin().then(function (response) {

    //    usuariosSrv.guardarUsuarioActual(response.data);

    //    $scope.filtro = angular.copy(documentosSrv.filtroActual());


    //    $scope.listadoPaginado($scope.filtro);

    //    $scope.maxSize = 5;

    //    $scope.currentPage = 1;


    //});// fin usuario


    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;


    };








    var formdata = new FormData();

    $scope.getTheFiles = function ($files) {

        angular.forEach($files, function (value, key) {

            formdata.set(key, value);
        });

    };

    // sube la información es decir losa archivos con la identificación del informe

    $scope.uploadFiles = uploadFiles;


    function uploadFiles() {

        console.log("Este es el documento subido");
        console.log($scope.filtro);

        formdata.append("id_Sistema", 9);
        formdata.append("id_tipo_documento", 9);
        formdata.append("id_expediente", $scope.filtro.documento_buscado.Id_Expediente);

        documentosSrv.uploadFiles(formdata).then(function (response) {


            console.log(response);

            $scope.buscar();


        },
            // funcion en caso de error
            function (response) {

            });

    }



    $scope.eliminarDocumento = eliminarDocumento;

    function eliminarDocumento(idDocumento) {

        documentosSrv.eliminar(idDocumento).then(function (response) {

            $scope.buscar();


        });

    }



       
}


app.controller("tareaOperadorCtrl", ["$scope", "expedientesSrv", "tareasCamundaSrv", "tareasSrv", "usuariosSrv", "$routeParams", "$location", "empresasSrv","tipoExpedientesSrv",TareaOperadorCtrl]);

function TareaOperadorCtrl($scope, expedientesSrv, tareasCamundaSrv, tareasSrv, usuariosSrv, $routeParams, $location, empresasSrv, tipoExpedientesSrv) {


    $scope.tarea = {};
    $scope.vista = '';

    var revisor = {};

    var empresa = {};

    var expediente = {};

    $scope.tipos = [];

    // escucha el evento totalDocumentos de tal manera que cuando no hay documentos se deshabilita el boton de enviar a SERVIU

    $scope.$on("totalDocumentos", function (event, total) {

        $scope.totalDocumentos = total;

    });


    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        if (nuevoTab === "Documentos" || nuevoTab==="Modificar") {

            $scope.$broadcast("Expediente", $scope.expediente);
           
        }


    };

    $scope.esTab = function (tab) {

        return $scope.tab === tab;

    };

    $scope.estableceTab('Descripcion');



    tipoExpedientesSrv.listadoGeneral().then(function (respuesta) {

        $scope.tipos = respuesta.data.listado;


    });



    empresasSrv.porRut(0).then(function (response) {

        empresa = response.data.listado[0]; 

        tareasSrv.porId($routeParams.idTarea).then(function (respuesta) {

            $scope.tarea = respuesta.data.listado[0];

            expedientesSrv.porId($scope.tarea.Id_Expediente).then(function (respuesta) {

                $scope.expediente = respuesta.data.listado[0];

                $scope.expediente.Id_Tipo_Expediente = $scope.expediente.Id_Tipo_Expediente.toString();


                expediente = new Expediente(respuesta.data.listado[0]);

                console.log("La tarea es");
                console.log($scope.tarea.Id_Tarea);


                console.log("El expediente es");

                console.log($scope.expediente);

                usuariosSrv.porRut($scope.expediente.Rut_Revisor_Social).then(function (respuesta) {

                    revisor = respuesta.data;

                    console.log("Este es el revisor");

                    console.log(revisor);


                });

               
            }); // fin expedientes

        }); // fin tareas
               
    }); // fin empresa

   
    $scope.iniciarEnviar = function () {

        $scope.vista = 'dialogo';


    };

    $scope.cancelar = function () {

        $scope.vista = '';

    };

    $scope.enviando_expediente = false;

    $scope.enviar = function () {

         if ($scope.tarea.Observaciones === undefined) {

            $scope.tarea.Observaciones = "Sin Observaciones";

        }

        $scope.tarea.Id_Estado_Expediente = 2; // enviado

        $scope.expediente.Id_Estado_Expediente = 2; // enviado

        expedientesSrv.actualizar($scope.expediente).then(function (respuesta) {

            console.log("esta es la respuesta al actualizar el expediente");

            console.log(respuesta);

            $scope.enviando_expediente = true;

            tareasSrv.agregar($scope.tarea).then(function (respuesta) {

                console.log("esta es la respuesta al agregar la tarea");

                console.log(respuesta);

                var correo = new CorreoEnviado(new CorreoSPPV(empresa, revisor, expediente)).json();

             
                console.log("este es el correo");

                console.log(correo);

                console.log("este es el expediente como tabla");

                console.log(expediente.string());

                var data = {
                    "id": $scope.tarea.Id_Tarea,

                    "variables": {

                    "Id_Estado_Expediente": { "value": $scope.expediente.Id_Estado_Expediente.toString(), "type": "String" },

                        "correo_subsanado": { "value": JSON.stringify(correo), "type": "String" }
                    }
                };

                tareasCamundaSrv.completar(data).then(function (respuesta) {

                    console.log(respuesta);
                    $scope.enviando_expediente = false;
                    $location.path("/proyectos/" + $scope.expediente.Id_Proyecto + "/expedientes");

                });


                //var data = {
                //    "variables": {

                //        "Id_Estado_Expediente": { "value": $scope.expediente.Id_Estado_Expediente.toString(), "type": "String" },

                //        "correo_subsanado": { "value": JSON.stringify(correo), "type": "String" }
                //    }
                //};

                

                //tareasProcesoSrv.completar($scope.tarea.Id_Tarea, data).then(function (respuesta) {

                   

                //    $location.path("/proyectos/" + $scope.expediente.Id_Proyecto +"/expedientes");

                //});

            });// fin agregar tarea

        });// fin actualizar expediente

    };

    $scope.volver = function () {

        $location.path("/proyectos/" + $scope.expediente.Id_Proyecto + "/expedientes");

    };

  
}


