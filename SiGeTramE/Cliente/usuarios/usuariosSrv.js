﻿
app.factory("usuariosSrv", ["$http", "urlApi" ,UsuariosSrv]);

function UsuariosSrv($http, urlApi) {

    var usuarioAct = {};


    var ruta = urlApi + "Usuarios/";


    var guardarUsuarioActual = function (usuarioActual) {

        usuarioAct = usuarioActual;
    };

    var usuarioActual = function () {

        return usuarioAct;
    };

    var porRut = function (rut) {

        return $http.get(ruta + rut);
    };
       
    var porLogin = function () {

        return $http.get(ruta+"login");
    };


    var actualizar = function (usuario) {


        return $http.put(ruta, usuario);
    };

    var agregar = function (usuario) {

        return $http.post(ruta, usuario);
    };

    var eliminar = function (usuario) {

        return $http.delete(ruta + usuario.Rut);
    };

    var porRol = function (idRol) {

        return $http.get(ruta + "/rol/" + idRol);
    };

    
   
   

    return {
        porRut: porRut,
        agregar: agregar,
        actualizar: actualizar,
        eliminar: eliminar,
        porRol: porRol,
        guardarUsuarioActual: guardarUsuarioActual,
        usuarioActual: usuarioActual,
        porLogin:porLogin
        
    };
}






