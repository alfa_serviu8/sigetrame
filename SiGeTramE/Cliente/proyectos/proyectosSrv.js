﻿app.factory("proyectosSrv", ["$http", "urlApi", "$localStorage", ProyectosSrv]);

function ProyectosSrv($http, urlApi, $localStorage) {

    var ruta = urlApi + "proyectos/";

    var rut = new Rut($localStorage.currentUser.username);
       
    var filtro = {
        proyecto: {
            "Id_Proyecto": 0,
            "Codigo_Proyecto": "",
            "Nombre_Proyecto": "",
            "Rut_Empresa": rut.rut(),
            "Nombre_Empresa": "",
            "Rut_Revisor": 0,
            "Rut_Operador": 0,
            "Id_Tipo_Subsidio": "0",
            "Id_Estado_Proyecto": "0",
            "Id_Tipologia": "0",
            "Id_Proceso": 0,
            "Id_Region": "0",
            "Id_Provincia": "0",
            "Id_Comuna": "0"
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };

    var porId = function (idProyecto) {
        var filtro = {
            proyecto: {
                "Id_Proyecto": idProyecto,
                "Codigo_Proyecto": "",
                "Nombre_Proyecto": "",
                "Rut_Empresa":0,
                "Nombre_Empresa": "",
                "Rut_Revisor": 0,
                "Rut_Operador": 0,
                "Id_Tipo_Subsidio": "0",
                "Id_Estado_Proyecto": "0",
                "Id_Tipologia": "0",
                "Id_Proceso": 0,
                "Id_Region": "0",
                "Id_Provincia": "0",
                "Id_Comuna": "0"
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }
        };

        return $http.post(ruta + "listado", filtro);

    }

    var guardar_filtro = function (nuevo_filtro) {
        filtro = nuevo_filtro;
    };

    var filtro_actual = function () {
        return filtro;
    };

    var listado = function (filtro) {
        return $http.post(ruta + "listado", filtro);
    };

    var agregar = function (proyecto) {
        return $http.post(ruta, proyecto);
    };

    var actualizar = function (proyecto) {
        return $http.put(ruta, proyecto);
    };

    var eliminar = function (Id_Proyecto) {

        return $http.delete(ruta, Id_Proyecto);
    };

    return {
        listado: listado,
        porId: porId,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual,
        agregar: agregar,
        actualizar: actualizar,
        eliminar: eliminar
    };
}
