﻿'use strict';




var app = angular.module('SiGeTramE', ['ngRoute', 'ui.bootstrap', 'gp.rutValidator', 'ngMessages', 'ngStorage']);


app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/', {
        controller: 'proyectosCtrl',
        templateUrl: 'Cliente/proyectos/vistas/frmProyectos.html'
    }).when('/tipoTramites', {
        controller: 'tipoTramitesCtrl',
        templateUrl: 'Cliente/tipoTramites/vistas/frmTipoTramites.html'
    }).when('/tipoTramites/:idTipoTramite/proyectos', {
        controller: 'proyectosCtrl',
        templateUrl: 'Cliente/proyectos/vistas/frmProyectos.html'
    }).when('/empresas', {
        controller: 'empresasCtrl',
        templateUrl: 'Cliente/empresas/vistas/frmEmpresas.html'
    }).when('/empresa/agregar', {
        controller: 'empresaAgregarCtrl',
        templateUrl: 'Cliente/empresas/vistas/frmEmpresaAgregar.html'
    }).when('/empresa/:Rut/editar', {
        controller: 'empresaEditarCtrl',
        templateUrl: 'Cliente/empresas/vistas/frmEmpresaEditar.html'
    }).when('/usuariosRol/:id_rol', {
        controller: 'usuariosRolCtrl',
        templateUrl: 'Cliente/roles/vistas/frmUsuariosRol.html'
    }).when('/usuariosRol/:id_rol/agregar', {
        controller: 'usuariosRolAgregarCtrl',
        templateUrl: 'Cliente/roles/vistas/frmUsuariosRolAgregar.html'

    }).when('/proyectos', {
        controller: 'proyectosCtrl',
        templateUrl: 'Cliente/proyectos/vistas/frmProyectos.html'

    }).when('/proyectos/agregar', {
        controller: 'proyectoAgregarCtrl',
        templateUrl: 'Cliente/proyectos/vistas/frmProyectoAgregar.html'

    }).when('/proyectos/:Id_Proyecto/editar', {
        controller: 'proyectoEditarCtrl',
        templateUrl: 'Cliente/proyectos/vistas/frmProyectoEditar.html'

    }).when('/proyectos/:Id_Proyecto/eliminar', {
        controller: 'proyectoEliminarCtrl',
        templateUrl: 'Cliente/proyectos/vistas/frmProyectoEliminar.html'

    }).when('/proyectos/:Id_Proyecto/expedientes', {
        controller: 'expedientesCtrl',
        templateUrl: 'Cliente/expedientes/vistas/frmExpedientes.html'
    }).when('/expedientes', {
            controller: 'expedientesCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedientes.html'
        }).when('/proyectos/:Id_Proyecto/expediente/agregar', {
        controller: 'expedienteEditarCtrl',
        templateUrl: 'Cliente/expedientes/vistas/frmExpedienteEditar.html'
        }).when('/proyectos/:Id_Proyecto/expediente/:Id_Expediente/editar', {
        controller: 'expedienteEditarCtrl',
        templateUrl: 'Cliente/expedientes/vistas/frmExpedienteEditar.html'
        }).when('/proyectos/:Id_Proyecto/expediente/:Id_Expediente/eliminar', {
            controller: 'expedienteEliminarCtrl',
            templateUrl: 'Cliente/expedientes/vistas/frmExpedienteEliminar.html'
        }).when('/proyectos/:Id_Proyecto/expediente/:Id_Expediente/ver', {
        controller: 'expedienteVerCtrl',
        templateUrl: 'Cliente/expedientes/vistas/frmExpedienteVer.html'
        }).when('/proyectos/:Id_Proyecto/expediente/:Id_Expediente/enviar', {
        controller: 'expedienteEnviarCtrl',
        templateUrl: 'Cliente/expedientes/vistas/frmExpedienteEnviar.html'
        }).when('/proyectos/:Id_Proyecto/expediente/:Id_Expediente/ver', {
        controller: 'expedienteVerCtrl',
        templateUrl: 'Cliente/expedientes/vistas/frmExpedienteVer.html'
        }).when('/proyectos/:Id_Proyecto/expediente/:Id_Expediente/documentos', {
        controller: 'documentosCtrl',
        templateUrl: 'Cliente/documentos/vistas/frmDocumentos.html'
   }).when('/proyectos/:Id_Proyecto/expediente/:id_expediente/documentos/ver', {
        controller: 'documentosCtrl',
        templateUrl: 'Cliente/documentos/vistas/frmDocumentosVer.html'
    }).when('/resoluciones/:id_Resolucion_Dispone/solicitudes', {
        controller: 'solicitudesAdmCtrl',
        templateUrl: 'Cliente/solicitudes/vistas/frmSolicitudesAdm.html'
    }).when('/proyectos/:Id_Proyecto/expediente/:id_expediente/modificar', {
        controller: 'expedienteModificarCtrl',
        templateUrl: 'Cliente/Expedientes/vistas/frmExpedienteModificar.html'
    }).when('/empresas/:rut_empresa/proyectos', {
        controller: 'proyectosCtrl',
        templateUrl: 'Cliente/proyectos/vistas/frmProyectos.html'
    }).when('/tipoSubsidios', {
        controller: 'tipoSubsidiosCtrl',
        templateUrl: 'Cliente/tipoSubsidios/vistas/frmTipoSubsidios.html'

    }).when('/tipoSubsidios/agregar', {
        controller: 'tipoSubsidioAgregarCtrl',
        templateUrl: 'Cliente/tipoSubsidios/vistas/frmTipoSubsidioAgregar.html'

    }).when('/tipoSubsidios/:Id_Tipo_Subsidio/editar', {
        controller: 'tipoSubsidioEditarCtrl',
        templateUrl: 'Cliente/tipoSubsidios/vistas/frmTipoSubsidioEditar.html'

    }).when('/tipoSubsidios/:Id_Tipo_Subsidio/eliminar', {
        controller: 'tipoSubsidioEliminarCtrl',
        templateUrl: 'Cliente/tipoSubsidios/vistas/frmTipoSubsidioEliminar.html'


    }).when('/tipoExpedientes', {
        controller: 'tipoExpedientesCtrl',
        templateUrl: 'Cliente/tipoExpedientes/vistas/frmTipoExpedientes.html'

    }).when('/tipoExpedientes/agregar', {
        controller: 'tipoExpedienteAgregarCtrl',
        templateUrl: 'Cliente/tipoExpedientes/vistas/frmTipoExpedienteAgregar.html'

    }).when('/tipoExpedientes/:Id_Tipo_Expediente/editar', {
        controller: 'tipoExpedienteEditarCtrl',
        templateUrl: 'Cliente/tipoExpedientes/vistas/frmTipoExpedienteEditar.html'

    }).when('/tipoExpedientes/:Id_Tipo_Expediente/eliminar', {
        controller: 'tipoExpedienteEliminarCtrl',
        templateUrl: 'Cliente/tipoExpedientes/vistas/frmTipoExpedienteEliminar.html'


    }).when('/modalidades', {
        controller: 'modalidadesCtrl',
        templateUrl: 'Cliente/modalidades/vistas/frmModalidades.html'

    }).when('/modalidades/agregar', {
        controller: 'modalidadAgregarCtrl',
        templateUrl: 'Cliente/modalidades/vistas/frmModalidadAgregar.html'

    }).when('/modalidades/:Id_Modalidad/editar', {
        controller: 'modalidadEditarCtrl',
        templateUrl: 'Cliente/modalidades/vistas/frmModalidadEditar.html'

    }).when('/modalidades/:Id_Modalidad/eliminar', {
        controller: 'modalidadEliminarCtrl',
        templateUrl: 'Cliente/modalidades/vistas/frmModalidadEliminar.html'

    }).when('/roles', {
        controller: 'rolCtrl',
        templateUrl: 'Cliente/roles/vistas/frmRoles.html'

    }).when('/login', {
        controller: 'loginCtrl',
        templateUrl: 'Cliente/autenticacion/login.html'

    }).when('/cambiarClave', {
            controller: 'cambiarClaveCtrl',
            templateUrl: 'Cliente/autenticacion/cambiarClave.html'

    });
}]);

app.run(run);

function run($rootScope, $http, $location, $localStorage) {
    // keep user logged in after page refresh
    if ($localStorage.currentUser) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.currentUser.token;
        $rootScope.autenticado = true;
    }

    // redirect to login page if not logged in and trying to access a restricted page
    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        var publicPages = ['/login'];

        var restrictedPage = publicPages.indexOf($location.path()) === -1;

              
        if (restrictedPage && !$localStorage.currentUser) {
            $location.path('/login');
        }
    });
}

app.controller('navegacionCtrl', ['$rootScope',NavegacionCtrl]);

function NavegacionCtrl($rootScope) {

   
    $rootScope.autenticado = false;

        

      

}




function Usuario(u) {

    this.rut = u.Rut;

    this.nombres = u.Nombres;

    this.id_servicio = u.Id_Servicio; // o será que tiene un objeto servicio asociado?

    this.logo_servicio = u.Logo_Servicio;

    this.nombre_servicio = u.Nombre_Servicio;


    this.Nombres = function () {

        return this.nombres;
    };

    this.Rut = function () {

        return this.rut;
    };

    this.Id_Servicio = function () {

        return this.id_servicio;
    };


} // fin usuario

function Roles(roles) {

    this.roles = roles;

    this.esAdministrador = function () {

        var es_administrador = false;

        if (this.roles.length > 0) {

            for (var i = 0; i < this.roles.length; i++) {

                if (this.roles[i].Id_Rol === 1) {

                    es_administrador = true;


                }// fin if

            }// fin for


        }
        return es_administrador;
    };

    this.esRevisorSocial = function () {

        var esRevisor = false;

        if (this.roles.length > 0) {

            for (var i = 0; i < this.roles.length; i++) {

                if (this.roles[i].Id_Rol === 5) {

                    esRevisor = true;


                }// fin if

            }// fin for


        }
        return esRevisor;
    };

    this.esOperador = function () {

        var esOperador = false;

        if (this.roles.length === 0) {


            esOperador = true;


        }
        return esOperador;
    };

}

app.factory("sistemaSrv", sistemaSrv);

function sistemaSrv() {

    function Sistema() {

        this.id_sistema = 22;

        this.numero = function () {

            return this.id_sistema;
        }//fin if

    }//fin administrador


    return Sistema;
}







////Local
app.constant("urlApi", "http://localhost:51446/api/");





////Desarrollo
//app.constant("urlApi", "http://intranet08.minvu.cl/PlaninEP/api/");
//app.constant("urlApiProceso", "http://10.208.18.100:8080/engine-rest/");
//app.constant("urlApiLogin", "http://intranet08.minvu.cl/PlaninEP/api/");
//app.constant("urlApiSigeFun", "http://intranet08.minvu.cl/SigeFun/Api/");

////Produccion
//app.constant("urlApi", "http://intranet08.minvu.cl/solcom/api/");
//app.constant("urlApiSigeFun", "http://intranet08.minvu.cl/SigeFun/Api/");
//app.constant("urlApiLogin", "http://intranet08.minvu.cl/webapilogin/api/");

////Internet
//app.constant("urlApi", "http://tramites.serviubiobio.cl/SiGeTramE/api/");
//app.constant("urlApiProceso", "http://10.208.18.100:8080/engine-rest/");


