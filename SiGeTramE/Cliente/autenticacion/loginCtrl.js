﻿
app.controller('loginCtrl', LoginCtrl);

function LoginCtrl($scope, $location, autenticacionSrv, $rootScope) {

    $scope.titulo = "Login";



    $scope.usuarioCambioClave = {};




    $scope.login = login;

    initController();

    function initController() {
        // reset login status
        autenticacionSrv.Logout();
    }


    $rootScope.autenticado = false;



    function login() {
        $scope.loading = true;


        //aquí se tiene que limpiar el username

        // $scope.username = $filter('rutCleanFormat')($scope.username);

        autenticacionSrv.Login($scope.username, $scope.password, function (result) {


            if (result === true) {

                $rootScope.autenticado = true;
                $location.path('/proyectos');
            } else {
                $scope.error = 'El Rut o password es incorrecto';
                $scope.loading = false;
            }
        });
    }
}

app.controller('cambiarClaveCtrl', CambiarClaveCtrl);

function CambiarClaveCtrl($scope, $location, autenticacionSrv) {

   
    function setVista(vista) {

        $scope.vista = vista;


    }

    $scope.usuarioCambioClave = {};

    setVista("cambiarClave");
    

    $scope.volver = function () {
               
        $location.path("/proyectos");

    };

    $scope.volverLogin = function () {

        $location.path("/login");

    };

       
    $scope.cambiarClave = function () {

        $scope.dataLoading2 = true;

        $scope.claveIgual = 'Si';


        if ($scope.usuarioCambioClave.clave1 === $scope.usuarioCambioClave.clave2) {

            $scope.claveIgual = 'Si';

            // guarda la clave y debiera mostrar un dialogo final de éxito una card


            autenticacionSrv.Actualizar($scope.usuarioCambioClave.userName, $scope.usuarioCambioClave.clave1, function (result) {



                console.log(result);

                setVista("dialogoClave");


            });

        } else {

            $scope.claveIgual = 'No';
            $scope.dataLoading2 = false;

        }



        // aquí debería guardar la clave


    };
           
     

    }







app.directive('dialogoClave', [DialogoClave]);




function DialogoClave() {
    return {
        restrict: 'E',
        templateUrl: 'Cliente/autenticacion/dialogoCambioClave.html'
    };
}