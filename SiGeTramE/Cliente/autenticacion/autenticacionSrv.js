﻿app.factory("autenticacionSrv", ["$http", "$localStorage","urlApi", Servicio]);

function Servicio($http, $localStorage, urlApi) {
        var service = {};

        service.Login = Login;
    service.Logout = Logout;
    service.Actualizar = Actualizar;
    service.UsuarioRegistrado = UsuarioRegistrado;

        return service;

        function Login(username, password, callback) {
            $http.post(urlApi +'login/authenticate', { username: username, password: password })
                .then(function (response) {
                    // login successful if there's a token in the response
                    console.log("este es el response de  la autenticacion");
                    console.log(response);


                    if (response.data) {
                        // store username and token in local storage to keep user logged in between page refreshes
                        $localStorage.currentUser = { username: username, token: response.data };

                        console.log("este es el log storage");
                        console.log($localStorage.currentUser);

                       

                        // add jwt token to auth header for all requests made by the $http service
                        $http.defaults.headers.common.Authorization = 'Bearer ' + response.data;


                        // execute callback with true to indicate successful login
                        callback(true);
                    } else {
                        // execute callback with false to indicate failed login
                        callback(false);
                    }
                });
        }

        function Logout() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
    }

    function Actualizar(username, password, callback) {
        // remove user from local storage and clear http auth header
        $http.put(urlApi + 'login', { username: username, password: password })
            .then(function (response) {

                console.log("validacion de usuario");
                console.log(response.data);
                callback(true);

            });
    }

    function UsuarioRegistrado(username, password, callback) {
        // remove user from local storage and clear http auth header
        $http.post(urlApi + 'login/userNameRegistrado', { username: username, password: password })
            .then(function (response) {

                if (response.data.usuarioRegistrado) {
                    callback(true);
                } else {

                    callback(false);
                }

            });
    }
}
