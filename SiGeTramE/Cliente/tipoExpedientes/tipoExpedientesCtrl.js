﻿app.controller("tipoExpedientesCtrl", ["$scope", "tipoExpedientesSrv", "tipoTramitesSrv", "usuariosSrv", "$localStorage", "$location", "$routeParams", TipoExpedientesCtrl]);

function TipoExpedientesCtrl($scope, tipoExpedientesSrv, tipoTramitesSrv, usuariosSrv,$localStorage,$location,$routeParams) {

    $scope.tipoExpedientes = [];



   
    $scope.filtro = tipoExpedientesSrv.filtro_actual();
    $scope.filtro.tipoExpediente.Id_Tipo_Tramite = $localStorage.Id_Tipo_Tramite;
  
    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    }
   
    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.filtro = angular.copy(tipoExpedientesSrv.filtro_actual());

        $scope.listadoPaginado(tipoExpedientesSrv.filtro_actual());

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }) // fin usuario
  

    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }


    $scope.listadoPaginado = function (filtro_actual) {

        //var filtro = angular.copy(filtro_actual);
        var filtro = filtro_actual;

        tipoExpedientesSrv.guardar_filtro(filtro);

        tipoExpedientesSrv.listado(filtro).then(function (respuesta) {

            $scope.tipoExpedientes = respuesta.data.pagina.tiposExpediente;

                    
            $scope.bigTotalItems = respuesta.data.pagina.total_registros;
        })
    }
}// fin controlador


app.controller("tipoExpedienteAgregarCtrl", ["$scope", "tipoExpedientesSrv", "tipoTramitesSrv", "$location", "$routeParams",TipoExpedienteAgregarCtrl]);

function TipoExpedienteAgregarCtrl($scope, tipoExpedientesSrv, tipoTramitesSrv,$location,$routeParams) {

    $scope.tipoExpediente = {
        Vigente:"1"
    };

    $scope.tipoTramites = [];

    tipoTramitesSrv.listadoGeneral().then(function (response) {

        $scope.tipoTramites = response.data.pagina.tiposTramite;

    })



    $scope.guardando_tipo_expediente = false;

    $scope.guardar = function () {

        tipoExpedientesSrv.agregar($scope.tipoExpediente).then(function (respuesta) {

            console.log(respuesta);

            $location.path("/tipoExpedientes");

            //console.log(respuesta);
        })
    }
}


app.controller("tipoExpedienteEditarCtrl", ["$scope", "tipoExpedientesSrv","tipoTramitesSrv", "$location", "$routeParams", TipoExpedienteEditarCtrl]);

function TipoExpedienteEditarCtrl($scope, tipoExpedientesSrv, tipoTramitesSrv, $location, $routeParams) {

    $scope.tipoTramites = [];

    tipoTramitesSrv.listadoGeneral().then(function (response) {

        $scope.tipoTramites = response.data.pagina.tiposTramite;

    })


    $scope.tipoExpediente = {};

    $scope.guardando_tipo_expediente = false;

    $scope.filtro = angular.copy(tipoExpedientesSrv.filtro_actual());

    $scope.filtro.tipoExpediente.Id_Tipo_Expediente = $routeParams.Id_Tipo_Expediente;

   

    tipoExpedientesSrv.listado($scope.filtro).then(function (respuesta) {

   
        $scope.tipoExpediente = respuesta.data.pagina.tiposExpediente[0];

        $scope.tipoExpediente.Vigente = $scope.tipoExpediente.Vigente.toString();

        $scope.tipoExpediente.Id_Tipo_Tramite = $scope.tipoExpediente.Id_Tipo_Tramite.toString();

    });



    $scope.guardar = function () {

        tipoExpedientesSrv.actualizar($scope.tipoExpediente).then(function (respuesta) {

            $location.path("/tipoExpedientes");
        });
    }
}