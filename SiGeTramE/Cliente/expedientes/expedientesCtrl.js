﻿
app.controller("expedientesCtrl", ["$scope", "expedientesSrv", "usuariosSrv", "$location", "$routeParams", "estadoExpedientesSrv", "tipoExpedientesSrv",  "sistemaSrv", ExpedientesCtrl]);

function ExpedientesCtrl($scope, expedientesSrv, usuariosSrv, $location, $routeParams, estadoExpedientesSrv, tipoExpedientesSrv, sistemaSrv) {

    $scope.expedientes = [];
    $scope.estados = [];
    $scope.tipos = [];
    $scope.usuarioActual = {};
    $scope.proyecto = {};
    $scope.filtro = {};


    $scope.pageChanged = function () {

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;
    };

    $scope.tamacno_cambio = function () {

        $scope.buscar();
    };

    estadoExpedientesSrv.listado().then(function (respuesta) {

        console.log("los estados");
        console.log(respuesta.data.listado);

        $scope.estados = respuesta.data.pagina.estadosExpediente;
    });

    tipoExpedientesSrv.listadoGeneral().then(function (respuesta) {

        $scope.tipos = respuesta.data.listado;
    });


   $scope.filtro = angular.copy(expedientesSrv.filtro_actual());


    console.log($routeParams.Id_Proyecto);

    if ($routeParams.Id_Proyecto) {

        $scope.filtro.expediente.Id_Proyecto = $routeParams.Id_Proyecto;

    } else {

        $scope.filtro.expediente.Id_Proyecto = 0;
    }


    $scope.agregar = function () {

        $location.path("proyectos/" + $routeParams.Id_Proyecto + "/expediente/agregar");
    };

    $scope.listadoPaginado = function (filtro_actual) {

        //var filtro = angular.copy(filtro_actual);
        var filtro = filtro_actual;

        expedientesSrv.guardar_filtro(filtro);

        expedientesSrv.listado(filtro).then(function (respuesta) {

            //$scope.expedientes = respuesta.data.pagina.expedientes;
            var i = 0;

            for (i = 0; i < respuesta.data.pagina.expedientes.length; i++) {
                if (respuesta.data.pagina.expedientes[i].Rut_Revisor >0 ) {
                    $scope.expedientes.push(respuesta.data.pagina.expedientes[i]);
                }

            }
            
            console.log("Estos son los expedientes");
            console.log($scope.expedientes);

            $scope.bigTotalItems = respuesta.data.pagina.totalRegistros;
        });
    };

    $scope.listadoPaginado($scope.filtro);

    $scope.maxSize = 5;

    $scope.currentPage = 1;

    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    };
    
    $scope.modificar = function (idExpediente) {

        

        $location.path("proyectos/" + $routeParams.Id_Proyecto + "/expediente/" + idExpediente + "/modificar");
       
    };

    $scope.volver = function () {

        $location.path("/proyectos");
    };

}


app.controller("expedienteEditarCtrl", ["$scope", "expedientesSrv", "$location", "$routeParams", "proyectosSrv","estadoExpedientesSrv","tipoExpedientesSrv","usuariosSrv", ExpedienteEditarCtrl]);

function ExpedienteEditarCtrl($scope, expedientesSrv, $location, $routeParams, proyectosSrv, estadoExpedientesSrv, tipoExpedientesSrv,usuariosSrv) {

    $scope.expediente = {
      };

    $scope.titulo = '';
    $scope.estados = [];
    $scope.tipos = [];
    $scope.totalDocumentos = 0;

    $scope.$on("totalDocumentos", function (event, total) {

        $scope.totalDocumentos = total;
    });

    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        if (nuevoTab === "Documentos") {

            $scope.$broadcast("Expediente", $scope.expediente);
        }
    };

    $scope.esTab = function (tab) {

        return $scope.tab === tab;
    };

    $scope.estableceTab('Descripcion');

    $scope.enviar = function () {

        $location.path("/proyectos/" + $scope.expediente.Id_Proyecto + "/expediente/" + $scope.expediente.Id_Expediente + "/enviar");
    };






    if ($routeParams.Id_Expediente) {
        $scope.expediente.Id_Expediente = $routeParams.Id_Expediente;
        $scope.titulo = "Modificar Expediente";
    } else {
        $scope.expediente.Id_Expediente = 0;
        $scope.expediente.Id_Proyecto = $routeParams.Id_Proyecto;
        $scope.expediente.Id_Estado_Expediente = "1";
        $scope.expediente.Id_Tipo_Expediente = "0";
        $scope.expediente.Descripcion = "";

        $scope.titulo = "Agregar expediente";

        console.log("El expediente deberia ser");
        console.log($scope.expediente);


        // trae el proyecto
       


    }

    $scope.proyecto = {};

    proyectosSrv.porId($routeParams.Id_Proyecto).then(function (respuesta) {

        $scope.proyecto = respuesta.data.pagina.proyectos[0];

        tipoExpedientesSrv.porProceso($scope.proyecto.Id_Proceso).then(function (respuesta) {

            $scope.tipos = respuesta.data.pagina.tiposExpediente;

            console.log("estos son los tipos");
            console.log(respuesta.data);
        });

    });
     
    estadoExpedientesSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosExpediente;
    });

   
   
       
    if ($scope.expediente.Id_Expediente > 0) {

        expedientesSrv.porId($scope.expediente.Id_Expediente).then(function (respuesta) {
            $scope.expediente = respuesta.data.pagina.expedientes[0];
            $scope.expediente.Id_Estado_Expediente = $scope.expediente.Id_Estado_Expediente.toString();
            $scope.expediente.Id_Tipo_Expediente = $scope.expediente.Id_Tipo_Expediente.toString();
            //$scope.expediente.Descripcion = $scope.expediente.Descripcion;



            if ($scope.expediente.Id_Estado_Expediente == 2 || $scope.expediente.Id_Estado_Expediente == 4) {

                $scope.titulo = "Ver expediente";
            }





        });
    } else {

        $scope.expediente.Id_Tipo_Expediente = "0";


    }

    $scope.guardando_expediente = false;
    
    $scope.guardar = function () {
        console.log("El expediente");
        console.log($scope.expediente);

        if ($scope.expediente.Id_Expediente > 0) {
            expedientesSrv.actualizar($scope.expediente).then(function (respuesta) {
            });
        } else


        {

            

            expedientesSrv.agregar($scope.expediente).then(function (respuesta) {

                $scope.expediente.Id_Expediente = respuesta.data.IdExpediente;

               
                $scope.$broadcast("Expediente", $scope.expediente);
            });
        }
    };


    $scope.volver = function () {

        $location.path("/proyectos/" + $routeParams.Id_Proyecto + "/expedientes");
    };
}

app.controller("expedienteEliminarCtrl", ["$scope", "expedientesSrv", "$location", "$routeParams", "documentosSrv", "estadoExpedientesSrv", "tipoExpedientesSrv", "usuariosSrv", ExpedienteEliminarCtrl]);

function ExpedienteEliminarCtrl($scope, expedientesSrv, $location, $routeParams, documentosSrv, estadoExpedientesSrv, tipoExpedientesSrv, usuariosSrv) {

    $scope.expediente = {
        
    };

   
    $scope.documentos = [];

    documentosSrv.porExpediente($routeParams.Id_Expediente).then(function (respuesta) {


        $scope.documentos = respuesta.data.pagina.documentos;

        if ($scope.documentos.length > 0) {

            $scope.vista = "no-puede-eliminar";

        } else {

            $scope.vista = "eliminar";

            expedientesSrv.porId($routeParams.Id_Expediente).then(function (respuesta) {

                $scope.expediente = respuesta.data.pagina.expedientes[0];


            });


        }


    });

    $scope.eliminando_expediente = false;

    $scope.eliminar = function () {

        $scope.eliminando_expediente = true;

        expedientesSrv.eliminar($routeParams.Id_Expediente).then(function (respuesta) {

            $scope.eliminando_expediente = false;
            $scope.volver();

        });


    };

    
    $scope.irDocumentos = function () {


        $location.path("/proyectos/" + $routeParams.Id_Proyecto + "/expediente/" + $routeParams.Id_Expediente + "/editar");
    };

    $scope.volver = function () {

        $location.path("/proyectos/" + $routeParams.Id_Proyecto + "/expedientes");
    };
}



app.controller("expedienteVerCtrl", ["$scope", "expedientesSrv", "$location", "$routeParams", "proyectosSrv", "estadoExpedientesSrv", "tipoExpedientesSrv", "usuariosSrv", ExpedienteVerCtrl]);

function ExpedienteVerCtrl($scope, expedientesSrv, $location, $routeParams, proyectosSrv, estadoExpedientesSrv, tipoExpedientesSrv, usuariosSrv) {

    $scope.expediente = {};
    $scope.titulo = '';
    $scope.estados = [];
    $scope.tipos = [];
    $scope.totalDocumentos = 0;

    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        if (nuevoTab !== "Descripcion") {

            $scope.$broadcast("Expediente", $scope.expediente);

            console.log("Distinto a Descripcion");
            console.log($scope.expediente);
        }
    };

    $scope.esTab = function (tab) {

        return $scope.tab === tab;

    };

    $scope.estableceTab('Descripcion');

    estadoExpedientesSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosExpediente;
    });

    


    expedientesSrv.porId($routeParams.Id_Expediente).then(function (respuesta) {

        
        console.log("este s el expediente");
        console.log(respuesta);
        console.log($routeParams.id_expediente);


        $scope.expediente = respuesta.data.pagina.expedientes[0];

        $scope.expediente.Id_Estado_Expediente = $scope.expediente.Id_Estado_Expediente.toString();
        $scope.expediente.Id_Tipo_Expediente = $scope.expediente.Id_Tipo_Expediente.toString();

        tipoExpedientesSrv.porProceso($scope.expediente.Id_Prceso).then(function (respuesta) {

            $scope.tipos = respuesta.data.pagina.tiposExpediente;

            console.log("estos son los tipos");
            console.log(respuesta.data);
        });





    });




        $scope.volver = function () {

            $location.path("/proyectos/" + $routeParams.Id_Proyecto + "/expedientes");

        };

    
}


app.controller("tareasHistorialCtrl", ["$scope", "expediente_tareaSrv", TareasHistorialCtrl]);

function TareasHistorialCtrl($scope, expediente_tareaSrv) {

    $scope.tareas = [];

    $scope.tarea = {};

    $scope.$on("Expediente", function (event, expediente) {


        expediente_tareaSrv.historial(expediente.Id_Expediente).then(function (respuesta) {

            $scope.tareas = respuesta.data.pagina.expedienteTareas;

            console.log(respuesta.data);

            $scope.bigTotalItems = respuesta.data.pagina.total_registros;



        });
    });


}



app.controller("expedienteEnviarCtrl", ["$scope", "expedientesSrv", "empresasSrv", "$location", "$routeParams", ExpedienteEnviarCtrl]);

function ExpedienteEnviarCtrl($scope, expedientesSrv, empresasSrv, $location, $routeParams) {
       
    var empresa = {};

    var filtro = {

        expediente: {
            "Id_Expediente": $routeParams.Id_Expediente,
            "Id_Tipo_Expediente": "",
            "Id_Estado_Expediente": "",
            "Id_Proyecto": "0",
            "Descripcion": ""
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };

    $scope.expediente = {};
    var expediente = {};
    var revisor = {};

    // pero aquí tenemos que ver como se va hacer con el operador

    empresasSrv.porRut(0).then(function (response) {

          empresa = response.data.pagina.empresas[0]; 

          expedientesSrv.listado(filtro).then(function (respuesta) {

            $scope.expediente = respuesta.data.pagina.expedientes[0];

           
              
        });
    });

    $scope.enviando_expediente = false;

    console.log($scope.enviando_expediente);

    $scope.enviar = function () {

       
        //var correo = new CorreoEnviado(new CorreoSPPV(empresa, revisor, expediente)).json();
        //var correoIngresado = new CorreoAEmpresa(empresa,expediente).json();

        

        $scope.enviando_expediente = true;

        $scope.expediente.Id_Estado_Expediente = 2;
       

        

        expedientesSrv.enviar($scope.expediente).then(function (respuesta) {


            $scope.enviando_expediente = false;

            $location.path("/proyectos/" + $routeParams.Id_Proyecto + "/expedientes");

        })

        
        
      

      
       


        
    };

    $scope.enviarModificado = function () {

    };

    $scope.volver = function () {

        $location.path("/proyectos/" + $routeParams.Id_Proyecto + "/expediente/" + $routeParams.Id_Expediente + "/editar");
    };
}

app.controller("expedienteModificarCtrl", ["$scope", "expediente_tareaSrv", "tipoExpedientesSrv", "$routeParams", "$location", "empresasSrv", ExpedienteModificarCtrl]);

function ExpedienteModificarCtrl($scope, expediente_tareaSrv, tipoExpedientesSrv, $routeParams, $location, empresasSrv) {



    $scope.tarea = {};

    $scope.vista = '';

    var revisor = {};

    var empresa = {};

    var expediente = {};

    $scope.tipos = [];

    // escucha el evento totalDocumentos de tal manera que cuando no hay documentos se deshabilita el boton de enviar a SERVIU

    $scope.$on("totalDocumentos", function (event, total) {

        $scope.totalDocumentos = total;

    });


    $scope.estableceTab = function (nuevoTab) {

        $scope.tab = nuevoTab;

        if (nuevoTab === "Documentos" || nuevoTab === "Modificar") {

            $scope.$broadcast("Expediente", $scope.expediente_tarea);

        }


    };

    $scope.esTab = function (tab) {

        return $scope.tab === tab;

    };

    $scope.estableceTab('Descripcion');



   

    expediente_tareaSrv.porId($routeParams.id_expediente).then(function (respuesta) {

        $scope.expediente_tarea = respuesta.data.pagina.expedienteTareas[0];

        $scope.expediente_tarea.Id_Tipo_Expediente = $scope.expediente_tarea.Id_Tipo_Expediente.toString();
    
        console.log("El expediente es");

        console.log($scope.expediente_tarea);

        console.log($routeParams);

        tipoExpedientesSrv.porProceso($scope.expediente_tarea.Id_Proceso).then(function (respuesta) {

            $scope.tipos = respuesta.data.pagina.tiposExpediente;
        });
               

    }); // fin expedientes

    $scope.iniciarEnviar = function () {

        $scope.vista = 'dialogo';


    };

    $scope.cancelar = function () {

        $scope.vista = '';

    };

    $scope.enviando_expediente = false;

    $scope.enviar = function () {

        $scope.enviando_expediente = true;
           
        expediente_tareaSrv.modificar($scope.expediente_tarea).then(function (respuesta) {

            $scope.enviando_expediente = false;

            $scope.volver();
               
        });

      

    };

    $scope.volver = function () {

        $location.path("/proyectos/" + $routeParams.Id_Proyecto + "/expedientes");

    };



}

   
function CorreoSPPV(remitente, destino, expediente) {
        this.de = "planin@minvu.cl";
        this.alias = "PlanIn";
        this.url_tareas = "http://intranet08.minvu.cl/planin/#!/proyectos/" + expediente.json.Id_Proyecto + "/expedientes";
        this.url_expedientes = "http://intranet08.minvu.cl/planin/#!/expedientes";
        this.expediente = expediente;
        this.remitente = remitente;
        this.destino = destino;

        this.json = function () {
            var c = {
                De: "planin@minvu.cl",
                Alias: "PlanIn",
                Hacia: this.destino.Email,
                //Hacia:"asaez@minvu.cl",
                Asunto: this.expediente.como_asunto_correo(),
                Body: "<div style=\"font-family:Verdana; font-size:12px;\">" + new Saludo(this.destino).string()
           };

            return c;
        };
    }

function CorreoAEmpresa(destino, expediente) {
    this.de = "planin@minvu.cl";
    this.alias = "PlanIn";
    this.expediente = expediente;
    this.destino = destino;

    this.json = function () {
        var c = {
            De: "planin@minvu.cl",
            Alias: "PlanIn",
            Hacia: this.destino.Email,
            //Hacia:"asaez@minvu.cl",
            Asunto: this.expediente.como_asunto_correo(),
            Body: "<div style=\"font-family:Verdana; font-size:12px;\">" + new SaludoEmpresa(this.destino).string() + this.expediente.string() + "<br/><br/>" +
                "Dentro del plazo de 05 días hábiles contados desde esta fecha, se hará recepción del expediente o se le notificarán observaciones, según corresponda. <br/><br/>" +
                "Atte.</br>" +
                "SERVIU Región del Biobio"

        };

        return c;
    };
}



function SaludoEmpresa(empresa) {

    this.empresa = empresa;

    this.string = function () {

        var saludo = '';

        saludo = 'Entidad Patrocinante ' + empresa.Razon_Social + ' ha enviado el siguiente expediente:<br/><br/>';

        return saludo;
    };

}

    function CorreoEnviado(origen) {

        this.origen = origen;

        this.json = function () {

            var c = this.origen.json();

            c.Body = c.Body + this.origen.remitente.Razon_Social + " le ha enviado un expediente que debe ser recepcionado o devuelto con observaciones por usted según su criterio. <br/><br/> " +
                this.origen.expediente.string() + " <br/>Para dar respuesta a esta solicitud pinche <a href='" + this.origen.url_tareas + "'>Aquí.</a><br/> </div>";

            return c;
        };
    }


function CorreoRecepcionado(origen) {

    this.origen = origen;

    this.json = function () {

        var c = this.origen.json();

        c.Body = c.Body + this.origen.remitente.NombreCompleto + " ha recepcionado el expediente:. <br/><br/> " +

            this.origen.expediente.string();

        return c;
    };
}

function CorreoIngresado(origen) {

    this.origen = origen;

    this.json = function () {

        var c = this.origen.json();

        c.Body = c.Body  +

            this.origen.expediente.string();

        return c;
    };
}



function CorreoObservado(origen) {

    this.origen = origen;

    this.json = function () {

        var c = this.origen.json();

        c.Body = c.Body + this.origen.remitente.NombreCompleto + " ha observado el expediente:. <br/><br/> " +

            this.origen.expediente.string();

        return c;
    };
}


function Saludo(persona) {

        this.persona = persona;

        this.string = function () {

            var saludo = '';

            if (this.persona.Id_Genero === 1) {

                saludo = 'Estimada ' + persona.Nombres + ':<br/><br/>';

            } else {

                saludo = 'Estimado ' + persona.Nombres + ':<br/><br/>';
            }

            return saludo;
        };
}
