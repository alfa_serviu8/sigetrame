﻿app.factory("empresasSrv", ["$http", "urlApi","$localStorage", EmpresasSrv]);

function EmpresasSrv($http, urlApi, $localStorage) {

    var ruta = urlApi + "empresas/";

    var filtro = {
        empresa: {
            "Rut_Empresa": 0,
             "Nombre_Empresa": "",
            "Pass_Sistemas": ""
            
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };

    var guardar_filtro = function (filtro) {

        filtro = filtro;
    };

    var filtro_actual = function () {

        return filtro;
    };

    var listado = function (filtro) {
        
        return $http.post(ruta + "listado", filtro);
    };


    var porRut = function () {

        var rut = new Rut($localStorage.currentUser.username);

        var filtro = {
            empresa: {
                "Rut_Empresa": rut.rut(),
                "Nombre_Empresa": "",
                "Pass_Sistemas": "",
                "Es_Empresa_Patrocinante": -1
                
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }

        };
        console.log("este es elflitro en srv");
        console.log(filtro);



        return listado(filtro);
    };

    var listadoGeneral = function () {

        var filtro = {
            empresa: {
                "Rut_Empresa": 0,
                "Nombre_Empresa": "",
                "Pass_Sistemas": "",
                "Es_Empresa_Patrocinante":-1
                
            },
            pagina: {
                "numero": "1",
                "tamacno": "1000"
            }

        };

        return listado(filtro);

    }

    var agregar = function (empresa) {

        return $http.post(ruta, empresa);
    };
  
    var actualizar = function (empresa) {

        return $http.put(ruta, empresa);
    };

    var resetearClave = function (empresa) {

        return $http.put(ruta + "resetearClave", empresa);
    };



    var eliminar = function (rut) {

        return $http.delete(ruta + rut);
    };

    return {
        listado: listado,
        listadoGeneral: listadoGeneral,
        porRut: porRut,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual,
        agregar: agregar,
        actualizar: actualizar,
        resetearClave: resetearClave,
        eliminar:eliminar
    };
}

function Rut(rutConDv) {

    this.rutConDv = rutConDv;


    this.dv = function () {

        return this.rutConDv.substring(this.rutConDv.length - 1);

    };

    this.rut = function () {

        return this.rutConDv.substring(0, this.rutConDv.length - 2);

    };

}

