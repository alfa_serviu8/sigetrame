﻿app.controller("tipoTramitesCtrl", ["$scope", "tipoTramitesSrv", "usuariosSrv", "$location", "$routeParams", "$localStorage", TipoTramitesCtrl]);

function TipoTramitesCtrl($scope, tipoTramitesSrv, usuariosSrv, $location, $routeParams, $localStorage) {

    $scope.tipoTramites = [];



   
    $scope.filtro = tipoTramitesSrv.filtro_actual();
  
    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    }
   
    $scope.listadoPaginado = function (filtro_actual) {

        //var filtro = angular.copy(filtro_actual);
        var filtro = filtro_actual;

        tipoTramitesSrv.guardar_filtro(filtro);

        tipoTramitesSrv.listado(filtro).then(function (respuesta) {

            $scope.tipoTramites = respuesta.data.pagina.tiposTramite;


            $scope.bigTotalItems = respuesta.data.pagina.total_registros;
        })
    }

        $scope.filtro = angular.copy(tipoTramitesSrv.filtro_actual());

        $scope.listadoPaginado(tipoTramitesSrv.filtro_actual());

        $scope.maxSize = 5;

        $scope.currentPage = 1;
   
  

    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }

    $scope.guardarTramite = function (tipoTramiteSeleccionado) {

        $localStorage.tipoTramite = tipoTramiteSeleccionado;

        $location.path("/proyectos/");
    }

    
}// fin controlador


