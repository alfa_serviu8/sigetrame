﻿app.factory("tipoSubsidiosSrv", ["$http", "urlApi", TipoSubsidiosSrv]);

function TipoSubsidiosSrv($http, urlApi) {

    var ruta = urlApi + "tipoSubsidio/";

    var filtro = {
        tipoSubsidio: {
            "Id_Tipo_Subsidio": 0,
            "Codigo_Tipo_Subsidio": "",
            "Nombre_Tipo_Subsidio": "",
            "Vigente": -1
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }

    };

    var guardar_filtro = function (filtro) {
        filtro = filtro;
    };

    var filtro_actual = function () {
        return filtro;
    };

    var listado = function (filtro) {
        return $http.post(ruta + "listado", filtro);
    };

    var listadoGeneral = function () {

        var filtro = {
            tipoSubsidio: {
                "Id_Tipo_Subsidio": 0,
                "Codigo_Tipo_Subsidio": "",
                "Nombre_Tipo_Subsidio": "",
                "Vigente": -1
            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }

        };

        return $http.post(ruta + "listado", filtro);
    };


    var agregar = function (tiposubsidio) {
        return $http.post(ruta, tiposubsidio);
    };
  
    var actualizar = function (tiposubsidio) {
        return $http.put(ruta, tiposubsidio);
    };
       
    var eliminar = function (Id_Tipo_Subsidio) {

        return $http.delete(ruta, Id_Tipo_Subsidio);
    };

    return {
        listado: listado,
        listadoGeneral: listadoGeneral,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual,
        agregar: agregar,
        actualizar: actualizar,
        eliminar: eliminar
    };

}



