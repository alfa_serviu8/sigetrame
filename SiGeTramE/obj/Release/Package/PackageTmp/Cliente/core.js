﻿'use strict';


// Formulario

function Formulario(form) {

    if (form === "") {
        this.form = document.createElement("FORM");
    } else {
        this.form = form;
       
    }

    this.con = function(componente) {

       

        var formulario = this.form;

        // aqui agrego algo al formulario
        formulario.appendChild(componente.html());

        return new Formulario(formulario);

    };

    this.html = function() {
                                     
        this.form.className = "form";
                   

        return this.form;
    };

}



//Encabezado

function CompEncabezado(titulo) {

    this.titulo = titulo;

    this.html = function () {

        var div = document.createElement("DIV");

        var h2 = document.createElement("H2");

        var br = document.createElement("BR");

        h2.id = "tablas";

        h2.style.marginTop = "-60px;";

        h2.className = "titles-site";

        h2.appendChild(document.createTextNode(this.titulo));

        div.appendChild(h2);

        div.appendChild(br);

        return div;

    };

}



// divs



function DivRow(origen) {

    this.origen = origen;


    this.html = function () {


        var div = document.createElement("div");

        div.className = "row";

        return div;

    };
}


function DivContainer(origen) {

    this.origen = origen;


    this.html = function () {


        var div = this.origen.html();

        div.className = "container";

        return div;

    };
}


function DivCol(origen, dimension) {

    this.origen = origen;

    this.dimension = dimension;

    this.html = function () {


        var div = this.origen.html();

        div.className = "col col-sm-" + this.dimension.toString();

        return div;

    };
}

function GrupoInput(id,nombreLabel,placeHolder) {

    this.id = id;

    this.nombreLabel = nombreLabel;

    this.placeHolder = placeHolder;
    
    this.html = function () {

        var label = document.createElement("label");

        label.className = "class-control-label";

        label.setAttribute("for", this.id);

        label.appendChild(document.createTextNode(this.nombreLabel));

        var input = document.createElement("input");

        input.className = "form-control";

        input.id = this.id;

        input.name = this.id;

        input.setAttribute("placeholder", this.placeHolder);

        var div = document.createElement("div");

        div.className = "form-group";

        div.appendChild(label);

        div.appendChild(input);

        return div;

    };

}








function Ancla(id) {

    this.id = id;

    this.html = function () {

        var a = document.createElement("a");

        a.id = id;

        a.href = "";

        return a;

    };

}

function AnclaEditar(origen) {

    this.origen = origen;

    this.html = function () {

        var ancla = this.origen.html();

        var iEditar = document.createElement('i');

        iEditar.className = "fa fa-edit fa-2x";

        iEditar.title = this.origen.id;

        ancla.appendChild(iEditar);

        return ancla;

    };


}


function Boton(id,nombre) {

    this.id = id;

    this.nombre = nombre;

    this.html = function (obj) {

        var boton = document.createElement("button");

        boton.className = "btn btn-primary";

        boton.id = this.id;

        boton.type = "button";

        boton.appendChild(document.createTextNode(this.nombre));

        boton.onclick = function () {

            obj.hola();

        };

        return boton;
    };
}



function URL() {


    this.planin = function () {

        return "http://localhost:64553/api/fuentedatos/";

    };

    this.comun = function () {

        return "http://rrhh.intranet08.minvu.cl/webapilogin/api/usuarios/";

    };
}

function FuenteDatos(json, url) {

    this.json = json;
    this.url = url;

    this.enviar = function (resultado) {
        $.ajax({
            type: "POST",
            url: this.url.planin(),
            data: JSON.stringify(this.json), success: function (resultado, status) {

                resultado = resultado;

            },
            contentType: "application/json"
        });

    };
}


function Root() {

    this.root = document.getElementById("root");

    this.agregar = function (elemento) {

         

        this.root.appendChild(elemento);

    };

    this.eliminar = function (elemento) {

        root.removeChild(root.childNodes[0]);


    };


}