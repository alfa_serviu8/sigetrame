﻿app.factory("tipoTramitesSrv", ["$http", "urlApi", TipoTramitesSrv]);

function TipoTramitesSrv($http, urlApi) {

    var ruta = urlApi + "tipoTramite/";

    var filtro = {
        tipoTramite: {
            "Id_Tipo_Tramite": 0,
            "Vigente": -1
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }

    };



    var guardar_filtro = function (nuevofiltro) {
        console.log("Este es el filtro");
        console.log(nuevofiltro);
                 filtro = nuevofiltro;
    };

    var filtro_actual = function () {
        return filtro;
    };

    var listado = function (filtro) {
        return $http.post(ruta + "listado", filtro);
    };

    var listadoGeneral = function () {
        var filtro = {
            tipoTramite: {
                "Id_Tipo_Tramite": 0,
                "Vigente": -1
            },
            pagina: {
                "numero": "1",
                "tamacno": "100"
            }

        };

      
        return listado(filtro);
    };


    var agregar = function (tipoTramite) {
        return $http.post(ruta, tipoTramite);
    };
  
    var actualizar = function (tipoTramite) {
        return $http.put(ruta, tipoTramite);
    };
       
    var eliminar = function (Id_Tipo_Tramite) {

        return $http.delete(ruta, Id_Tipo_Tramite);
    };

    return {
        listado: listado,
        listadoGeneral: listadoGeneral,
        guardar_filtro: guardar_filtro,
        filtro_actual: filtro_actual,
        agregar: agregar,
        actualizar: actualizar,
        eliminar: eliminar
    };

}



