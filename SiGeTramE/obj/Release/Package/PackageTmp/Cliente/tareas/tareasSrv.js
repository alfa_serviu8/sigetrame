﻿app.factory("tareasSrv", ["$http", "urlApi", TareasSrv]);

function TareasSrv($http, urlApi) {

    var ruta = urlApi + "tareas/";
   
    var filtro = {
         tareaBuscada: {
                "Id_Tarea": 0,
                "Rut_Asignado": 0,
                "Id_Expediente": 0

            },
            "numero_pagina": "1",
            "tamacno_pagina": "10"
        
    };

    var guardarFiltro = function (filtro) {
        filtro = filtro;
    };

    var filtroActual = function () {
        return filtro;
    };

    var listado = function (filtro) {
        return $http.post(ruta + "listado", filtro);
    };

    var porId = function (idTarea) {

        var filtro = {
            tareaBuscada: {
                "Id_Tarea": idTarea,
                "Rut_Asignado": 0,
                "Id_Expediente": 0

            },
            "numero_pagina": "1",
            "tamacno_pagina": "10"


        };

        return listado(filtro);

    };

    var agregar = function (tarea) {

        return $http.post(ruta, tarea);
    };

    var actualizar = function (tarea) {

        return $http.put(ruta, tarea);
    };
       
      

    return {
        listado: listado,
        porId:porId,
        guardarFiltro: guardarFiltro,
        filtroActual: filtroActual,
        agregar: agregar,
        actualizar: actualizar
                
    };
}



app.factory("tareasProcesoSrv", ["$http", "urlApiProceso", TareasProcesoSrv]);

function TareasProcesoSrv($http, urlApiProceso) {


    var actuales = function (data) {

        var request = {
            method: 'POST',
            url: urlApiProceso + 'task/',
            data: data,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': undefined
            }
        };


        return $http(request);

    };


    var ruta_formulario = function (Id_Tarea) {

        //return $http.get(urlApiProceso + "task/" + Id_Tarea + "/form");
        var request = {
            method: 'GET',
            url: urlApiProceso + "task/" + Id_Tarea + "/form",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': undefined
            }
        };

        return $http(request);
    };


    var listado = function (data) {

        var request = {
            method: 'POST',
            url: urlApiProceso + 'history/task',
            data: data,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': undefined
            }
        };


        return $http(request);

    };

    var PorId = function (id) {

        return $http.get(urlApiProceso + "task?id=" + idProceso);

    };




    var completar = function (id, data) {

        var request = {
            method: 'POST',
            url: urlApiProceso + 'task/' + id + '/complete',
            data: data,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': undefined
            }
        };


        return $http(request);

    };


    var historia = function (idInstanciaProceso) {

        return $http.get(urlApiProceso + "history/task?processInstanceId=" + idInstanciaProceso);

    }

    var variables = function (idTarea, nombreVariable) {

        return $http.get(urlApiProceso + "task/" + idTarea + "/variables/" + nombreVariable);

    }


    return {
        actuales: actuales,
        completar: completar,
        historia: historia,
        listado: listado,
        PorId: PorId,
        variables: variables,
        ruta_formulario: ruta_formulario
    };

}

app.factory("tareaSrv", TareaSrv);

function TareaSrv() {

    function Tarea(id, nombre, id_Vb_Jefatura, observaciones) {

        this.Id_Tarea = Id;
        this.Nombre = Nombre;
        this.Id_Vb_Jefe = Id_Vb_Jefe;
        this.Observaciones = observaciones

    }

    return Tarea;
}



app.factory("estadosTareaSrv", ["$http", "urlApi", EstadosTareaSrv]);

function EstadosTareaSrv($http, urlApi) {

    var ruta = urlApi + "estadosTarea/";

    var Listado = function () {

        return $http.get(ruta);
    };

   

    return {
        Listado: Listado
       
    };
}



