﻿'use strict';

angular.module('Authentication')

.controller('LoginController',
    ['$scope', '$rootScope', '$location', 'AuthenticationService','usuariosSrv','$filter',
    function ($scope, $rootScope, $location, AuthenticationService,usuariosSrv,$filter) {
        // reset login status


        function setVista(vista) {

            $scope.vista = vista;

        }


        setVista("login");

        AuthenticationService.ClearCredentials();

        $scope.obtenerClave = function () {

            setVista("obtenerClave");

        }

        $scope.obtenerUsuario = function () {

            $scope.dataLoading2 = true;

            $scope.rutUsuario = $filter('rutCleanFormat')($scope.rutUsuario);

            AuthenticationService.UsuarioRegistrado($scope.rutUsuario, function (response) {

                if (response.data.success) {

                    var usuario = {};

                    $scope.claveIgual = 'Si';

                    usuario = response.data.usuarios[0];



                    if ($scope.clave1 == $scope.clave2) {

                        $scope.claveIgual = 'Si';

                        // guarda la clave y debiera mostrar un dialogo final de éxito una card


                        usuario.Clave = $scope.clave1;

                        usuariosSrv.actualizar(usuario).then(function (response) {


                            console.log(response);

                            setVista("dialogoClave");


                        })

                    } else {

                        $scope.claveIgual = 'No';

                    }


                    
                    // aquí debería guardar la clave


                } else {
                    $scope.error = response.message;
                      }

                $scope.dataLoading2 = false;

            })

        }

        $scope.volver = function () {

            setVista("login");
            
        }


        $scope.login = function () {
            $scope.dataLoading = true;

            $scope.username = $filter('rutCleanFormat')($scope.username);

            AuthenticationService.Login($scope.username, $scope.password, function (response) {
               

                 if (response.data.success) {

                     AuthenticationService.SetCredentials($scope.username, $scope.password);
                     usuariosSrv.guardarUsuarioActual(response.data.usuarios[0]);
                     $rootScope.usuarioActual = response.data.usuarios[0];
                     $rootScope.soyAdministrador = usuariosSrv.soyAdministrador();
                     console.log($rootScope.soyAdministrador);
                     
                    $location.path('/');
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
            });
        };
        }])
    .directive('obtenerClave', [ObtenerClave])
    .directive('dialogoClave', [DialogoClave]);

function ObtenerClave() {
    return {
        restrict: 'E',
        templateUrl: 'Cliente/modulos/authentication/views/obtenerClave.html'
    };
};



function DialogoClave() {
    return {
        restrict: 'E',
        templateUrl: 'Cliente/modulos/authentication/views/dialogoClaveCorrecta.html'
    };
};