﻿
app.factory("documentosSrv", ["$http", "urlApi", DocumentosSrv]);

function DocumentosSrv($http, urlApi) {

    var ruta = urlApi + "Documentos/";

    var filtro = {
        documento: {
            "Id_Expediente": 0
            
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }

    };



    var guardarFiltro = function (filtro) {

        filtro = filtro;

    };

    var filtroActual = function () {

        return filtro;
    };

    var porExpediente = function (idExpediente) {
        var filtro = {
            documento: {
                "Id_Expediente": idExpediente

            },
            pagina: {
                "numero": "1",
                "tamacno": "10"
            }

        };

        return listado(filtro);
    };
   
    var eliminar = function (idDocumento) {
        return $http.delete(ruta + idDocumento);
    };

    var listado = function (filtro) {

        return $http.post(ruta +"listado" , filtro);
    };

    var uploadFiles = function (form) {


        var request = {
            method: 'POST',
            url: urlApi + '/documentos/',
            data: form,
            headers: {
                'Content-Type': undefined
            }
        };

        return $http(request);
    };

    var bajar = function (idDocumento) {

        return $http.get(ruta + idDocumento, { responseType: 'arraybuffer' });
    };

    


    

    return {
        eliminar: eliminar,
        uploadFiles: uploadFiles,
        listado: listado,
        porExpediente: porExpediente,
        bajar: bajar,
        guardarFiltro: guardarFiltro,
        filtroActual:filtroActual
    };

}

function Legible(maxKb, maxMb) {
    this.maxKb = maxKb;
    this.maxMb = maxMb;
    
    this.valor = function (cantidad) {
        var valor = "";
        if (cantidad > this.maxKb) {
            valor = (cantidad * 0.000977).toFixed(2).toString()+ " Kb";
        }
        if (cantidad > this.maxMb) {
            valor = (cantidad * 0.0000009537).toFixed(2).toString() + " Mb";
        }

        return valor;
    };

   
}



app.directive('ngFiles', ['$parse', function ($parse) {

    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });
    };

    return {
        link: fn_link
    };
}]);

function ToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
}


function validarNombre(nombre) {
    console.log(nombre);

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < nombre.length)) {
        if (nombre.charAt(cont) == " ") {
            espacios = true;
           
        }

        cont++;
    }

    if (espacios) {
        alert("El nombre del archivo no puede contener espacios. Use '_' como alternativa");
        return false;
    }

    return espacios;



}
