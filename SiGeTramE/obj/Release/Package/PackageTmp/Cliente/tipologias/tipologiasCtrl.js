﻿app.controller("tipologiasCtrl", ["$scope", "tipologiasSrv", "tipoSubsidiosSrv", "usuariosSrv", "$location", "$routeParams", TipologiasCtrl]);

function TipologiasCtrl($scope, tipologiasSrv,tipoSubsidiosSrv,usuariosSrv,$location,$routeParams) {

    $scope.tipologias = [];

    $scope.subsidios = [];

    tipoSubsidiosSrv.listadoGeneral().then(function (respuesta) {

        $scope.subsidios = respuesta.data.pagina.tiposSubsidio;

        console.log("Estos son los subsidios");
        console.log($scope.subsidios);
    })

    $scope.filtro = {
        tipologia: {
            "Id_Tipologia": 0,
            "Nombre_Tipologia": "",
            "Id_Tipo_Subsidio": "0"
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };


    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.pagina.numero = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
        // buscar la pagina
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    }
   
    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.filtro = angular.copy(tipologiasSrv.filtro_actual());

        $scope.listadoPaginado(tipologiasSrv.filtro_actual());

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }) // fin usuario
  

    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }


    $scope.listadoPaginado = function (filtro_actual) {

        //var filtro = angular.copy(filtro_actual);
        var filtro = filtro_actual;

        tipologiasSrv.guardar_filtro(filtro);

        tipologiasSrv.listado(filtro).then(function (respuesta) {

            $scope.tipologias = respuesta.data.pagina.tipologias;
                    
            $scope.bigTotalItems = respuesta.data.pagina.total_registros;
        })
    }
}// fin controlador


app.controller("tipologiaAgregarCtrl", ["$scope", "tipologiasSrv", "tipoSubsidiosSrv", "$location", "$routeParams",TipologiaAgregarCtrl]);

function TipologiaAgregarCtrl($scope, tipologiasSrv,tipoSubsidiosSrv,$location,$routeParams) {

    $scope.tipologia = {
        Id_Tipologia: 0,
        Nombre_Tipologia: "",
        Id_Tipo_Subsidio: ""
    };

    var filtro = tipoSubsidiosSrv.filtro_actual();

    $scope.subsidios = [];

    tipoSubsidiosSrv.listado(filtro).then(function (respuesta) {

        //console.log(respuesta);

        $scope.subsidios = respuesta.data.pagina.tiposSubsidio;
    })

    $scope.guardando_tipologia = false;

    $scope.guardar = function () {

        //console.log($scope.tipologia);

        tipologiasSrv.agregar($scope.tipologia).then(function (respuesta) {

            $location.path("/tipologias");

            //console.log(respuesta);
        })
    }
}


app.controller("tipologiaEditarCtrl", ["$scope", "tipologiasSrv", "tipoSubsidiosSrv", "$location", "$routeParams", TipologiaEditarCtrl]);

function TipologiaEditarCtrl($scope, tipologiasSrv,tipoSubsidiosSrv, $location, $routeParams) {

    $scope.tipologia = {};

    var filtro = tipoSubsidiosSrv.filtro_actual();

    $scope.subsidios = [];

    tipoSubsidiosSrv.listado(filtro).then(function (respuesta) {

        //console.log(respuesta);

        $scope.subsidios = respuesta.data.pagina.tiposSubsidio;
    })

    $scope.guardando_tipologia = false;

    $scope.filtro = angular.copy(tipologiasSrv.filtro_actual());

    $scope.filtro.tipologiaBuscada.Id_Tipologia = $routeParams.Id_Tipologia;

   
    tipologiasSrv.listado($scope.filtro).then(function (respuesta) {
   
        $scope.tipologia = respuesta.data.pagina.tipologias[0];

        console.log(respuesta);

        $scope.tipologia.Id_Tipo_Subsidio = $scope.tipologia.Id_Tipo_Subsidio.toString();

    });


    $scope.guardar = function () {

        console.log($scope.tipologia);

        tipologiasSrv.actualizar($scope.tipologia).then(function (respuesta) {

            $location.path("/tipologias");
        });
    }
}