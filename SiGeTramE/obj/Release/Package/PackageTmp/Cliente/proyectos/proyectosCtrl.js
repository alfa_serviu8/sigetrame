﻿app.controller("proyectosCtrl", ["$scope", "proyectosSrv",  "tipoSubsidiosSrv", "usuariosSrv", "estadoProyectosSrv", "$location", "$routeParams", "empresasSrv", ProyectosCtrl]);

function ProyectosCtrl($scope, proyectosSrv, tipoSubsidiosSrv, usuariosSrv, estadoProyectosSrv, $location, $routeParams, empresasSrv) {

    $scope.proyectos = [];

    console.log("Este es el parametro de entrada");
    console.log($routeParams);

    var filtro = tipoSubsidiosSrv.filtro_actual();

    $scope.subsidios = [];

    tipoSubsidiosSrv.listado(filtro).then(function (respuesta) {

        $scope.subsidios = respuesta.data.pagina.tiposSubsidio;
        console.log("estos son los subsidios");
        console.log($scope.subsidios);
    })

    $scope.estados = [];

    estadoProyectosSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.pagina.estadosProyecto;
    })

   
    var filtro = {
        proyecto: {
            "Id_Proyecto": 0,
            "Codigo_Proyecto": "",
            "Nombre_Proyecto": "",
            "Rut_Empresa": 0,
            "Nombre_Empresa": "",
            "Rut_Revisor": 0,
            "Id_Tipo_Subsidio": "0",
            "Id_Estado_Proyecto": "0",
            "Id_Tipologia": "0",
            "Id_Proceso":0,
            "Id_Region": "0",
            "Id_Provincia": "0",
            "Id_Comuna": "0"
        },
        pagina: {
            "numero": "1",
            "tamacno": "10"
        }
    };

    $scope.pageChanged = function () {

        console.log('Page changed to: ' + $scope.currentPage);

        $scope.filtro.numero_pagina = $scope.currentPage;

        $scope.listadoPaginado($scope.filtro);
    };

    $scope.setPage = function (pageNo) {

        $scope.currentPage = pageNo;

        console.log('Pgina seteada a: ' + $scope.currentPage);
    };

    $scope.tamacno_cambio = function () {

        console.log('Page changed to 2: ' + $scope.currentPage);

        $scope.buscar();
    }

    empresasSrv.porRut().then(function (response) {
                

        $scope.filtro = angular.copy(proyectosSrv.filtro_actual());

        console.log("Esta es la empresa");
        console.log(response);

        $scope.filtro.proyecto.Rut_Empresa = response.data.pagina.empresas[0].Rut_Empresa;

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    });



    $scope.buscar = function () {

        $scope.listadoPaginado($scope.filtro);

        $scope.maxSize = 5;

        $scope.currentPage = 1;
    }


    $scope.listadoPaginado = function (filtro_actual) {

        var filtro = filtro_actual;

        proyectosSrv.guardar_filtro(filtro);

        console.log("Este es el filtro");
        console.log(filtro);

        proyectosSrv.listado(filtro).then(function (respuesta) {
            console.log("Este son lod proyectos");
            console.log(respuesta);
            $scope.proyectos = respuesta.data.pagina.proyectos;

            $scope.bigTotalItems = respuesta.data.pagina.total_registros;
        })
    }
}


app.controller("proyectoAgregarCtrl", ["$scope", "proyectosSrv", "tipoSubsidiosSrv", "usuariosSrv", "estadoProyectosSrv", "regionSrv", "provinciaSrv", "comunaSrv", "$location", "$routeParams", ProyectoAgregarCtrl]);

function ProyectoAgregarCtrl($scope, proyectosSrv, tipoSubsidiosSrv, usuariosSrv, estadoProyectosSrv, regionSrv, provinciaSrv, comunaSrv, $location, $routeParams) {

    $scope.proyecto = {
        Id_Proyecto: 0,
        Codigo_Proyecto: "",
        Nombre_Proyecto: "",
        Rut_Empresa: "0",
        Rut_Revisor_Social: "0",
        Id_Estado_Proyecto: "0",
        Id_Tipo_Subsidio: "0",
        Cantidad_Familias: "0",
        Id_Region: "0",
        Id_Provincia: "0",
        Id_Comuna: "0"//,
        //Id_Modalidad: "0"
    };

    var filtro = tipoSubsidiosSrv.filtro_actual();

    $scope.subsidios = [];

    tipoSubsidiosSrv.listado(filtro).then(function (respuesta) {

        $scope.subsidios = respuesta.data.listado;
    })

    $scope.estados = [];

    estadoProyectosSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.listado;
    })

    $scope.regiones = [];
    $scope.provincias = [];
    $scope.comunas = [];

    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        $scope.proyecto.Id_Region = usuariosSrv.usuarioActual().Id_Region.toString();

        regionSrv.Listado().then(function (respuesta) {

            $scope.regiones = respuesta.data;
            $scope.lista_de_provincias();
        })
    }) 

    $scope.lista_de_provincias = function () {

        provinciaSrv.PorRegion($scope.proyecto.Id_Region).then(function (respuesta) {
            $scope.provincias = respuesta.data;

            $scope.lista_de_comunas();
        })

        if ($scope.proyecto.Id_Region != usuariosSrv.usuarioActual().Id_Region) {

            $scope.proyecto.Id_Provincia = "0";
        }
        else {

            $scope.proyecto.Id_Provincia = usuariosSrv.usuarioActual().Id_Provincia.toString();
        }
    }

    $scope.lista_de_comunas = function () {

        comunaSrv.Listado($scope.proyecto.Id_Provincia).then(function (respuesta) {
            $scope.comunas = respuesta.data;
        })
    }

    $scope.guardando_proyecto = false;

    $scope.guardar = function () {

        proyectosSrv.agregar($scope.proyecto).then(function (respuesta) {

            $location.path("/proyectos");
        })
    }
}


app.controller("proyectoEditarCtrl", ["$scope", "proyectosSrv", "tipoSubsidiosSrv", "usuariosSrv", "estadoProyectosSrv", "regionSrv", "provinciaSrv", "comunaSrv", "$location", "$routeParams", ProyectoEditarCtrl]);

function ProyectoEditarCtrl($scope, proyectosSrv, tipoSubsidiosSrv, usuariosSrv, estadoProyectosSrv, regionSrv, provinciaSrv, comunaSrv, $location, $routeParams) {

    $scope.proyecto = {};

    $scope.subsidios = [];
    $scope.estados = [];
    $scope.regiones = [];
    $scope.provincias = [];
    $scope.comunas = [];

    $scope.guardando_proyecto = false;

    var filtro = tipoSubsidiosSrv.filtro_actual();

    tipoSubsidiosSrv.listado(filtro).then(function (respuesta) {

        $scope.subsidios = respuesta.data.listado;
    })

    estadoProyectosSrv.listado().then(function (respuesta) {

        $scope.estados = respuesta.data.listado;
    })

    usuariosSrv.porLogin().then(function (response) {

        usuariosSrv.guardarUsuarioActual(response.data);

        regionSrv.Listado().then(function (respuesta) {

            $scope.regiones = respuesta.data;
        })
    })

    $scope.lista_de_provincias = function () {

        provinciaSrv.PorRegion($scope.proyecto.Id_Region).then(function (respuesta) {
            $scope.provincias = respuesta.data;
            $scope.proyecto.Id_Provincia = "0";
            $scope.lista_de_comunas();
        })
    }

    $scope.lista_de_comunas = function () {

        comunaSrv.Listado($scope.proyecto.Id_Provincia).then(function (respuesta) {
            $scope.comunas = respuesta.data;
            $scope.proyecto.Id_Comuna = "0";
        })
    }

    $scope.filtro = angular.copy(proyectosSrv.filtro_actual());

    $scope.filtro.proyectoBuscado.Id_Proyecto = $routeParams.Id_Proyecto;

    proyectosSrv.listado($scope.filtro).then(function (respuesta) {

        $scope.proyecto = respuesta.data.listado[0];

        console.log(respuesta);

        $scope.proyecto.Codigo_Proyecto = $scope.proyecto.Codigo_Proyecto.toString();
        $scope.proyecto.Nombre_Proyecto = $scope.proyecto.Nombre_Proyecto.toString();
        $scope.proyecto.Rut_Empresa = $scope.proyecto.Rut_Empresa;
        $scope.proyecto.Rut_Revisor_Social = $scope.proyecto.Rut_Revisor_Social;
        $scope.proyecto.Id_Estado_Proyecto = $scope.proyecto.Id_Estado_Proyecto.toString();
        $scope.proyecto.Id_Tipo_Subsidio = $scope.proyecto.Id_Tipo_Subsidio.toString();
        $scope.proyecto.Cantidad_Familias = $scope.proyecto.Cantidad_Familias;
        $scope.proyecto.Id_Region = $scope.proyecto.Id_Region.toString();

        provinciaSrv.PorRegion($scope.proyecto.Id_Region).then(function (respuesta) {
            $scope.provincias = respuesta.data;
        })
        $scope.proyecto.Id_Provincia = $scope.proyecto.Id_Provincia.toString();

        comunaSrv.Listado($scope.proyecto.Id_Provincia).then(function (respuesta) {
            $scope.comunas = respuesta.data;
        })

        $scope.proyecto.Id_Comuna = $scope.proyecto.Id_Comuna.toString();
        //$scope.proyecto.Id_Modalidad = $scope.proyecto.Id_Modalidad.toString();
    });


    $scope.guardar = function () {

        console.log($scope.proyecto);

        proyectosSrv.actualizar($scope.proyecto).then(function (respuesta) {

            $location.path("/proyectos");
        });
    }
}


